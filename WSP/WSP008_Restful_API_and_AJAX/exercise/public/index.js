let title = document.querySelector('.title')
let words = title.textContent.split('')

// AJAX
let memo = document.querySelector('#memo-from')
memo.addEventListener('submit', (event) => {
  event.preventDefault();
  const form = event.target;
  let formObject = {};
  let formData = new FormData(form)
  // step 2
  const res = await fetch('/memos', {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(formObject)
  });

  // Clear the form here

});






title.textContent = ''
for (let word of words) {
  let div = document.createElement('div')
  div.className = 'word'
  div.textContent = word
  title.appendChild(div)
}

let memoList = document.querySelector('.memo-list')
let memoTemplate = memoList.querySelector('.memo')
memoTemplate.remove()

for (let memo of memos) {
  let memoContainer = memoTemplate.cloneNode(true)
  memoContainer.querySelector('.memo-content').textContent = memo.content
  let img = memoContainer.querySelector('img')
  if (memo.image) {
    img.src = '/uploads/' + memo.image
  } else {
    img.remove()
  }
  memoList.appendChild(memoContainer)
}

setTimeout(() => {
  // location.reload()
}, 2000)