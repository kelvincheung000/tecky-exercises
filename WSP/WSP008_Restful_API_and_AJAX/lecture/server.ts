import express from 'express'

let app = express()

app.get('/apples', async (req, res) => {
    res.json("ok");
});

app.post('/apples', async (req, res) => {
    console.log("req.params")
    res.json("ok");
});

app.get('/apples/:id', async (req, res) => {
    let idInput = req.params.id
    let id = Number(idInput)
    if (!id) {
        res.json("Invalid ID")
    }
    res.json("ok");
});

app.patch('/apples/:id', async (req, res) => {
    res.json("ok");
});

app.delete('/apples/:type/:from/:classType/:id', async (req, res) => {

    console.log(req.params)

    // let idInput = req.params.id;
    // let type = req.params.type;
    // let from = req.params.from;
    // let classType = req.params.classType;
    let { id, type, from, classType } = req.params;

    let id = Number(idInput)
    if (!id) {
        res.json("Invalid ID")
    }
    return
});

app.listen(8080, () => {
    console.log('Connected to http://localhost:8080')
})


// GET Ask the server to retrieve some resources (With or without id)
// POST Ask the server to create a new resource
// PATCH Ask the server to edit/update an existing resource (With id)
// DELETE Ask the server to remove an existing resource (With id)
