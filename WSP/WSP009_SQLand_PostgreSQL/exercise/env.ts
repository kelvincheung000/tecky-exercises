import dotenv from "dotenv";
dotenv.config();


const env = {
    DB_NAME: process.env.DB_NAME || "memo_wall",
    DB_USERNAME: process.env.DB_USERNAME || "postgres",
    DB_PASSWORD: process.env.DB_PASSWORD || "postgres",
};

export default env;