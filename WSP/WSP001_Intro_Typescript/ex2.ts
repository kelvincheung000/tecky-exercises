function findFactors(num: number): number[] {
    let factors: number[] = [];
    for (let factor = 2; factor <= num / 2; factor++) {
        if (num % factor === 0) {
            factors.push(factor);
        }
    }
    return factors;
}
console.log('factors of 2022:', findFactors(2022))

function leapYear(year: number): boolean {
    if (year % 400 === 0) {
        console.log("Leap Year");
        return true;
    } else if (year % 100 === 0) {
        console.log("Not a Leap Year");
        return false;
    } else if (year % 4 === 0) {
        console.log("Leap Year");
        return true;
    } else {
        console.log("Not a Leap Year");
        return false;
    }
}
console.log('is 1994 leap year?', leapYear(1994))
console.log('is 2000 leap year?', leapYear(2000))


function rnaTranscription(dna: string | Array<string>): string {
    let rna = "";
    for (let nucleotide of dna) {
        switch (nucleotide) {
            case "G":
                rna += "C";
                break;
            case "C":
                rna += "G";
                break;
            case "T":
                rna += "A";
                break;
            case "A":
                rna += "U";
                break;
            default:
                throw new Error(`The nucleotide ${nucleotide} does not exist`)
        }
    }
    return rna;
}
console.log(rnaTranscription('GCAT'))
console.log(rnaTranscription(['G', 'C', 'A', 'T']))



function factorial(number: number): number {
    if (number === 0 || number === 1) {
        return 1;
    }

    return factorial(number - 1) * number
}
console.log('factorial of 12', factorial(12))


// type Teacher = {
//     name: string
//     age: number
//     students: Array<Student>
// }

// type Student = {
//     name: string
//     age: number
//     exercises?: Array<Exercise>
// }

// type Exercise = {
//     score: number
//     date: Date
// }

// const peter: Teacher = {
//     name: 'Peter',
//     age: 50,
//     students: [
//         { name: 'Andy', age: 20 },
//         { name: 'Bob', age: 23 },
//         {
//             name: 'Charlie',
//             age: 25,
//             exercises: [{ score: 60, date: new Date('2019-01-05') }],
//         },
//     ],
// }

// console.log('the students of peter:', peter.students)

// let exercises: Exercise[] = []
// for (let student of peter.students) {
//     if (student.exercises) {
//         for (let exercise of student.exercises) {
//             exercises.push(exercise)
//         }
//     }
// }

// console.log('collected all exercises:', exercises)


type TimeoutHandler = () => void
const timeoutHandler: TimeoutHandler = () => {
    console.log("Timeout happens!");
}
const timeout: number = 1000;
setTimeout(timeoutHandler, timeout);




// const someValue: 12 | null = Math.random() > 0.5 ? 12 : null
// const nextValue = someValue = null ? null : someValue + 1
// console.log('nextValue:', nextValue)
