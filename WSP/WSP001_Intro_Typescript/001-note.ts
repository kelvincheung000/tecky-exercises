let color: string = "red" // color is a type of string

let numArray = [1, 2, 3, 4];

let val = some_function_call_that_can_return_the_value_or_null();
val.abc()
if (val) {
    val.abc();
}