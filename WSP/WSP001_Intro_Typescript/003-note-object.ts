let student = {
    name: "Peter",
    age: 30,
    dateOfBirth: new Date("1999-11-11")
}

type Characters = string
type Student = {
    exercises: any
    name: string,
    age: number,
    dateOfBirth: Date
}

// type SpecialName = `Alice` | `Bob`
type SpecialName = "Alice" | "Bob"
let myName: SpecialName "Charlie" // error format!!
