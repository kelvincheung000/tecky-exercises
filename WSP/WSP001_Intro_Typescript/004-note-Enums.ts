enum Direction {
    EAST,
    SOUTH,
    WEST,
    NORTH
}

enum Kind {
    Diamond,
    Club,
    Heart,
    Spade
}

function turnTo(direction: Direction) {
    if (direction == Direction.EAST) {
        console.log("This is the direction East!")
    }
    return direction != Direction.EAST;
}