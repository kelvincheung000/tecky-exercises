function add(a: number, b: number): number {
    return a + b;
}

function reduce(a: number, b: number) {
    return a - b;
}

function setTimeout(handler: (...args: any[]) => void, timeout: number): number
// The first argument is a handler function which can have arbitrary number of arguments ...args:any[]
// And the function is going to return void. 
// The timeout is a number that is the timeout value in mini-seconds.

