function sum(nums: Array<number>) {
    let total = 0
    for (let num of nums) {
        total += num;
    }
    return total;
}

// console.log(sub(["1", "2, "3]))  // error
console.log(sum([1, 2, 3, 4, 5]))
