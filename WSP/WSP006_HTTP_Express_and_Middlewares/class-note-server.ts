import express, { Request, Response } from 'express'
import { print } from 'listening-on'

let app = express()

let requestHandler = (req: Request, res: Response) => {
    console.log('req:', {
        method: req.method,
        url: req.url,
        path: req.path,
        query: req.query,
    })
    res.write('hello')
    let n = 10
    let id = req.query.id
    if (typeof id === 'string') {
        n = parseInt(id)
    }
    for (let i = 0; i < n; i++) {
        res.write('.')
    }
    res.end()
}

app.get('/hello', requestHandler)

// app.get('/game.html', (req, res) => {
//   res.end('content of game.html...')
// })

app.use(express.static('public'))

// the route handlers
let port = 8100
app.listen(port, () => {
    // console.log('listening on port http://localhost:' + port)
    print(port)
})
