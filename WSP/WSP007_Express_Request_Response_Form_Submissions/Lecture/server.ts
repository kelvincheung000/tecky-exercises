import express from 'express';
import { print } from 'listening-on'
import { api } from './public/api';
import { view } from './view';

export let app = express();

app.use(view)
app.use(api)

let port = 8100;
app.listen(port, () => {
    print(port)
})
