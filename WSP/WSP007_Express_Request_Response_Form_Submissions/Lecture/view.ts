import express from 'express';
import { students } from './data'

export let view = express.Router()

view.use(express.static('public'))

view.get('/students.html', (req, res) => {
    res.write(/* html */`<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Student List</title>
    </head>
    
    <body>
    <h1>Student List</h1>
    `)
    for (let student of students) {
        res.write(/* html */`
        <div>
        #${student.id}
        (${student.studentNumber})
        <b>${student.name}</b>
        <i>${student.subject}</i>
        </div>
        `)
    }

    res.write(/* html */ `
    <a href="/">Back to Home Page</a>
    </body>
    
    </html>`)
    res.end()
})



view.get('/students/:id.html', (req, res) => {
    let id = +req.params.id
    if (!id) {
        res.status(400).end(/* html */ `
        <p>invalid id in param</p>
        <a href='/'>Back to Home Page</a>
        `)
        return
    }
    let student = students.find(student => student.id === id)
    if (!student) {
        res.status(400).end(/* html */ `
        <p>student not found</p>
        <a href='/'>Back to Home Page</a>
        `)
        return
    }
    res.write(/* html */`<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Student List</title>
    </head>
    
    <body>
    <h1>Student Profile</h1>
    `)

    res.write(/* html */`
        <div>
        #${student.id}
        (${student.studentNumber})
        <b>${student.name}</b>
        <i>${student.subject}</i>
        </div>
        `)


    res.write(/* html */ `
    <a href="/">Back to Home Page</a>
    </body>
    
    </html>`)
    res.end()
})
