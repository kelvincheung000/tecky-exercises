
import jsonfile from 'jsonfile'
import { join } from 'path'
import { existsSync, mkdirSync } from 'fs'

let studentFile = join('data', 'students.json');

// if (!existsSync('data')) {
//     mkdirSync('data')
// }

mkdirSync('data', { recursive: true });

let nextStudentId = 1

export function getNextStudentId() {
    let id = nextStudentId
    nextStudentId++
    return id
}

export type Student = {
    id: number
    studentNumber: string
    name: string
    subject: string
}

export let students: Student[] = [
    { id: 1, studentNumber: 'P101', name: 'Alice', subject: 'Chinese' },
    { id: 2, studentNumber: 'P102', name: 'Bob', subject: 'English' },
    { id: 3, studentNumber: 'P103', name: 'Charlie', subject: 'Math' },
    { id: 4, studentNumber: 'P104', name: 'Dave', subject: 'Math' },
];

if (!existsSync(studentFile)) {
    students = jsonfile.readFileSync(studentFile)
}

students.forEach(
    student => (nextStudentId = Math.max(nextStudentId, student.id + 1)),
)

export function updateStudents(newStudents: Student[]) {
    students = newStudents
    jsonfile.writeFile(studentFile, students)
}