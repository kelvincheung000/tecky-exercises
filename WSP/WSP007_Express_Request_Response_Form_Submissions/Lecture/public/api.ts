import { students, Student, getNextStudentId, updateStudents } from '../data';
import express from 'express';

// CRUD
// C: Create
// R: Retrieval
// U: Update
// D: Delete

export let api = express.Router();

api.use(express.urlencoded({ extended: false }))

api.get('/students', (req, res) => {
    res.json(students);
})

api.get('/students/search', (req, res) => {
    let matches = students
    for (let _key in req.query) {
        let key = _key as keyof Student
        matches = matches.filter(student => student[key] === req.query[key])
    }
    res.json(matches);
})

api.get('/students/:id', (req, res) => {
    let id = +req.params.id
    if (!id) {
        res.status(400).json({ error: 'invalid id in param' })
        return
    }
    let student = students.find(student => student.id === id)
    if (!student) {
        res.status(404).json({ error: 'student not found' })
        return
    }
    res.json(student)
})

api.get('/students/:by/:value', (req, res) => {
    let by = req.params.by as keyof Student
    if (!by) {
        res.status(404).json({ error: 'missing "by" in params' })
        return
    }
    let value = req.params.value
    if (!value) {
        res.status(400).json({ error: 'missing value in params' })
        return
    }
    let student = students.find(student => student[by] === value)
    if (!student) {
        res.status(404).json({ error: 'student not found' })
        return
    }
    res.json(student)
})


api.post('/students', (req, res) => {
    console.log('registering student...')
    let { name, subject, studentNumber } = req.body
    if (!name) {
        res.status(400).json({ error: 'missing name in body' })
        return
    }
    if (!subject) {
        res.status(400).json({ error: 'missing subject in body' })
        return
    }
    if (!studentNumber) {
        res.status(400).json({ error: 'missing studentNumber in body' })
        return
    }
    let student = students.find(student => student.studentNumber === studentNumber)
    if (student) {
        res.status(409).json({ error: 'duplicated studentNumber in body' })
        return
    }
    let id = getNextStudentId()
    students.push({
        id, studentNumber, name, subject
    })
    updateStudents(students)
    // res.json({ id })
    // res.redirect(`/students/${id}`)
    res.redirect(`/students/${id}.html`)
})

api.patch('/students/:id', (req, res) => {
    let id = +req.params.id
    if (!id) {
        res.status(400).json({ error: 'invalid id in param' })
        return
    }
    let student = students.find(student => student.id === id)
    if (!student) {
        res.status(404).json({ error: 'student not found' })
        return
    }
    if (req.body.name) {
        student.name = req.body.name
    }
    if (req.body.subject) {
        student.subject = req.body.subject
    }
    updateStudents(students)
    res.json('updated')
})

api.delete('/students/:id', (req, res) => {
    let id = +req.params.id
    if (!id) {
        res.status(400).json({ error: 'invalid id in params' })
        return
    }
    updateStudents(students.filter(student => student.id !== id))
    res.json('deleted')
})
