let title = document.querySelector('.title')
let words = title.textContent.split('')
title.textContent = ''
for (let word of words) {
  let div = document.createElement('div')
  div.className = 'word'
  div.textContent = word
  title.appendChild(div)
}

let memoList = document.querySelector('.memo-list')
let memoTemplate = memoList.querySelector('.memo')
memoTemplate.remove()

for (let memo of memos) {
  let memoContainer = memoTemplate.cloneNode(true)
  memoContainer.querySelector('.memo-content').textContent = memo.content
  let img = memoContainer.querySelector('img')
  if (memo.image) {
    img.src = '/uploads/' + memo.image
  } else {
    img.remove()
  }
  memoList.appendChild(memoContainer)
}

setTimeout(() => {
  // location.reload()
}, 2000)
