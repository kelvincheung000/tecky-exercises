import express from 'express'
import formidable from 'formidable'
import { mkdirSync } from 'fs'
import { loadDataFile } from './data'

export let memoRoutes = express.Router()

let uploadDir = 'uploads'

mkdirSync(uploadDir, { recursive: true })

let form = formidable({
  uploadDir,
  allowEmptyFiles: false,
  maxFiles: 1,
  maxFileSize: 200 * 1024 ** 2,
  keepExtensions: true,
  filter: part => part.mimetype?.startsWith('image/') || false,
})

export type Memo = {
  id: number
  content: string
  image?: string
}
let memos = loadDataFile<Memo[]>('memo.json', [
  { id: 1, content: '網上連儂牆' },
  { id: 2, content: '香港加油' },
])
let nextMemoId = 1
for (let memo of memos.data) {
  nextMemoId = Math.max(nextMemoId, memo.id + 1)
}

memoRoutes.post('/memos', (req, res) => {
  form.parse(req, (err, fields, files) => {
    console.log('post memo:', { err, fields, files })
    if (err) {
      res.status(400).json({ error: String(err) })
      return
    }
    let content = fields.content
    if (typeof content !== 'string' || !content) {
      res
        .status(400)
        .json({ error: 'invalid content, expect an non-empty string' })
      return
    }
    let file = Array.isArray(files.image) ? files.image[0] : files.image
    let image = file.newFilename
    memos.data.push({
      id: nextMemoId,
      content,
      image,
    })
    nextMemoId++
    memos.save()
    // res.json('saved')
    res.redirect('/')
  })
})

memoRoutes.get('/memos', (req, res) => {
  res.json(memos.data)
})

memoRoutes.get('/memos.js', (req, res) => {
  let memoList = memos.data.sort((a, b) => b.id - a.id)
  res.setHeader('Content-Type', 'application/javascript')
  res.end(`let memos = ${JSON.stringify(memoList)}`)
})

memoRoutes.use('/uploads', express.static(uploadDir))
