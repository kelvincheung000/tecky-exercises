import express from "express";
import formidable from "formidable";
import { mkdirSync } from "fs";
import { loadDataFile } from "./data";
import { client } from "./db";
import { io } from "./server";

export let memoRoutes = express.Router();

let uploadDir = "uploads";

mkdirSync(uploadDir, { recursive: true });

let form = formidable({
    uploadDir,
    allowEmptyFiles: true,
    maxFiles: 1,
    maxFileSize: 200 * 1024 ** 2,
    keepExtensions: true,
    filter: (part) => part.mimetype?.startsWith("image/") || false,
});

export type Memo = {
    id: number;
    content: string;
    image?: string;
};
let memos = loadDataFile<Memo[]>("memo.json", [
    { id: 1, content: "網上連儂牆" },
    { id: 2, content: "香港加油" },
]);
let nextMemoId = 1;
for (let memo of memos.data) {
    nextMemoId = Math.max(nextMemoId, memo.id + 1);
}
memoRoutes.delete("/memos/:id", async (req, res) => {
    try {
        let memoId = Number(req.params.id);
        if (!memoId) {
            res.status(401).json({
                message: "Invalid ID",
            });
            return;
        }
        await client.query("delete from memos where id = $1", [memoId]);
        res.json({
            message: "Add memo success",
        });
        io.emit("should-get-new-memo");
    } catch (error) {
        res.status(500).json({
            message: "System error",
        });
    }
});

// to create a new memo
export let postMemo = (req: express.Request, res: express.Response) => {
    try {
        // form.parse <- handle request first, then call function declared in argument [1]
        form.parse(req, async (err, fields, files) => {
            console.log("post memo:", { err, fields, files });
            if (err) {
                res.status(400).json({ error: String(err) });
                return;
            }
            let content = fields.content;
            if (typeof content !== "string" || !content) {
                res.status(400).json({ error: "invalid content, expect an non-empty string" });
                return;
            }
            console.log("post memo ,", content);

            let file = Array.isArray(files.image) ? files.image[0] : files.image;
            let image = file ? file.newFilename : undefined;

            await client.query(
                "INSERT INTO memos (content,image, created_at, updated_at) values ($1,$2, current_timestamp, current_timestamp)",
                [content, image]
            );
            // memos.data.push({
            //   id: nextMemoId,
            //   content,
            //   image,
            // })
            // nextMemoId++
            // memos.save()
            res.json({
                message: "Add memo success",
            });
            io.emit("should-get-new-memo");
            // res.redirect('/')
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
};
memoRoutes.post("/memos", postMemo);
memoRoutes.get("/memos", async (req, res) => {
    let memoReusltFromDB = await client.query(/*sql*/ " select * from memos order by updated_at desc;");
    // console.table(memoReusltFromDB.rows);
    // let memoList = memos.data.sort((a, b) => b.id - a.id);
    res.json(memoReusltFromDB.rows);
});

memoRoutes.patch("/memos", async (req, res) => {
    let { id, content } = req.body;
    let memoId = Number(id);

    if (!memoId) {
        res.json({
            message: "Invalid memo id",
        });
    }
    console.log({ id, content });

    let updateResult = await client.query(
        /*sql */ `update memos set content = $1, updated_at = current_timestamp where id = $2`,
        [content, memoId]
    );
    console.log(updateResult.rows);

    //  let updatedMemos =  memos.data.map((memoItem)=>{
    //     if (memoItem.id  === memoId){
    //       return {
    //           id: memoId,
    //           content,
    //           image: memoItem.image
    //       };
    //     }else{
    //       return memoItem;
    //     }
    //   })

    //   memos.data = updatedMemos;
    //   memos.save()

    res.json({
        message: "updated success",
    });
});
memoRoutes.get("/memos.js", (req, res) => {
    let memoList = memos.data.sort((a, b) => b.id - a.id);
    res.setHeader("Content-Type", "application/javascript");
    res.end(`let memos = ${JSON.stringify(memoList)}`);
});

memoRoutes.use("/uploads", express.static(uploadDir));
