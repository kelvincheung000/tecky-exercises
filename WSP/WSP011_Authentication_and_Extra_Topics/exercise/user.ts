import express from "express";
import { loadDataFile } from "./data";
import { red, reset } from "asciichart";
import { client } from "./db";
import { checkPassword, hashPassword } from "./hash";
import fetch from 'node-fetch'
import { v4 as uuidv4 } from 'uuid';

export let userRoutes = express.Router();

userRoutes.use(express.urlencoded({ extended: false }));

export type User = {
    username: string;
    password: string;
};

let users = loadDataFile<User[]>("users.json", []);
if (users.data.length == 0) {
    console.error(red + "Error: missing users in users.json" + reset);
    process.exit(1);
}

userRoutes.get("/login/google", async (req, res) => {
    try {
        const accessToken = req.session?.['grant'].response.access_token;

        console.log("GOOGLE LOGIN handler!! :", accessToken);

        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });

        const result = await fetchRes.json();
        console.log('google result = ', result);

        const users = (await client.query(`SELECT * FROM users WHERE users.username = $1`, [result.email])).rows;
        const user = users[0];

        // if no user is found, help him/her to register a new account
        if (!user) {
            let randomString = uuidv4()
            let randomHashPassword = await hashPassword(randomString)
            await client.query(`
        INSERT INTO users (username, password, create_at, update_at) 
        values ($1, $2, current_timestamp, current_timestamp)`,
                [result.email, randomHashPassword],
            );
        }
        if (req.session) {
            req.session["username"] = result.email;
        }
        res.redirect("/");
    } catch (error) {
        res.status(500).json({
            message: 'Google login fail'
        })
    }
})

userRoutes.post("/login", async (req, res) => {
    try {
        let { username, password } = req.body;
        if (!username) {
            res.status(400).json({ error: "missing username" });
            return;
        }
        if (!password) {
            res.status(400).json({ error: "missing password" });
            return;
        }
        let userResult = await client.query(/*sql */ `select * from users where username = $1`, [username]);
        let dbUser = userResult.rows[0];

        console.log('dbUser = ', dbUser)
        console.log({ username, password })

        // let user = users.data.find(user => user.username === username)
        if (!dbUser) {
            res.status(404).json({ error: "user not found" });
            return;
        }

        let isMatched = await checkPassword(password, dbUser.password);

        if (!isMatched) {
            res.status(403).json({ error: "wrong password" });
            return
        }

        // if (dbUser.password !== password) {
        //     res.status(403).json({ error: "wrong password" });
        //     return;
        // }

        req.session["username"] = username;
        res.json({
            message: "login successfully",
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal system error",
        });
    }

    // res.redirect('/')
});

userRoutes.get("/me", (req, res) => {
    let currentUser = req.session["username"] ? req.session["username"] : "NA";
    res.json({
        user: currentUser,
    });
});
userRoutes.post("/logout", (req, res) => {
    req.session.destroy((error) => {
        if (error) {
            console.error("failed to destroy session:", error);
        }
        res.redirect("/");
    });
});
