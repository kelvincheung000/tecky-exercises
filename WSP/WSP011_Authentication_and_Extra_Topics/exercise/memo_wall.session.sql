CREATE TABLE memos(
    id SERIAL primary key,
    content TEXT not null,
    image VARCHAR(255),
    create_at TIMESTAMP not null,
    update_At TIMESTAMP not null
);
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) not null,
    password VARCHAR(255) not null,
    create_at TIMESTAMP not null,
    update_At TIMESTAMP not null
);
select *
from memos;