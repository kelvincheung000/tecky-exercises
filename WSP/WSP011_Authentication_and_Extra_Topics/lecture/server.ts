import express, { NextFunction, Response, Request } from 'express'
import expressSession from 'express-session'
import { readFileSync } from 'fs'
import { print } from 'listening-on'
import { cyan, reset } from 'asciichart'
import { join, resolve } from 'path'
import { memoRoutes } from './memo'
import { userRoutes } from './user'
import {connectDB} from './db'
import http from 'http';
import {Server as SocketIO} from 'socket.io';
let app = express()
const server = new http.Server(app);
export const io = new SocketIO(server);

app.use(express.json())

app.use(
  expressSession({
    secret: readFileSync('secret.txt').toString() ,
    resave: true,
    saveUninitialized: true,
  }),
)

declare module 'express-session' {
  interface SessionData {
    counter?: number
    username?: string
  }
}

app.use((req, res, next) => {
  let counter = req.session.counter || 0
  counter++
  req.session.counter = counter

  // this is optional, it should auto save
  // But for realtime update in concurrent requests, better call the save method explicitly
  req.session.save()

  next()
})

function d2(x: number): string {
  if (x < 10) {
    return '0' + x
  }
  return String(x)
}

app.use((req, res, next) => {
  let date = new Date()
  let y = date.getFullYear()
  let m = d2(date.getMonth() + 1)
  let d = d2(date.getDate())
  let H = d2(date.getHours())
  let M = d2(date.getMinutes())
  let S = d2(date.getSeconds())
  let counter = req.session.counter
  console.log(
    `[${cyan}${y}-${m}-${d} ${H}:${M}:${S}${reset}] (${counter}) ${req.method} ${req.url}`,
  )
  next()
})
let adminGuard = (req: Request, res: Response, next: NextFunction) => {
    if (req.session?.username) {
        next();
    } else {
        res.status(401).end("This resources is only accessible by admin");
        // res.end('')
    }
};


connectDB()


app.use(memoRoutes);
app.use(userRoutes);



io.on('connection', function (socket) {
  console.log(socket.id + " is connected to io server");
});
app.use(express.static('public'))
app.use('/admin', adminGuard, express.static('admin'))



app.use((req, res) => {
  res.sendFile(resolve(join('public', '404.html')))
})

let port = 8100
server.listen(port, () => {
  print(port)
})
