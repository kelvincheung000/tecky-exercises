import express from "express";
import { loadDataFile } from "./data";
import { red, reset } from "asciichart";
import { client } from "./db";
import { checkPassword } from "./hash";

export let userRoutes = express.Router();

userRoutes.use(express.urlencoded({ extended: false }));

export type User = {
    username: string;
    password: string;
};

let users = loadDataFile<User[]>("users.json", []);
if (users.data.length == 0) {
    console.error(red + "Error: missing users in users.json" + reset);
    process.exit(1);
}

userRoutes.post("/login", async (req, res) => {
    try {
        let { username, password } = req.body;
        if (!username) {
            res.status(400).json({ error: "missing username" });
            return;
        }
        if (!password) {
            res.status(400).json({ error: "missing password" });
            return;
        }

        let userResult = await client.query(/*sql */ `select * from users where username = $1`, [username]);
        let dbUser = userResult.rows[0];

        console.log('dbUser = ', dbUser)
        console.log({ username, password } )

        // let user = users.data.find(user => user.username === username)
        if (!dbUser) {
            res.status(404).json({ error: "user not found" });
            return;
        }
        let isMatched = await checkPassword(password,dbUser.password )

        if (!isMatched) {
            res.status(403).json({ error: "wrong password" });
            return;
        }
        req.session["username"] = username;
        res.json({
            message: "login successfully",
        });
    } catch (error) {
        res.status(500).json({
          message: "Internal system error",
      });
    }

    // res.redirect('/')
});

userRoutes.get("/me", (req, res) => {
    let currentUser = req.session["username"] ? req.session["username"] : "NA";
    res.json({
        user: currentUser,
    });
});
userRoutes.post("/logout", (req, res) => {
    let username = req.session['username']
    console.log(`${username} want to logout`)
    req.session.destroy((error) => {
        if (error) {
            console.error("failed to destroy session:", error);
        }
        
        res.redirect(`/index.html?user=${username}`);
    });
});
