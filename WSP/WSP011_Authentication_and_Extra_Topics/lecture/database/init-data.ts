import pg from "pg";
import XLSX from "XLSX";
import env from "../env";
import { hashPassword } from "../hash";

const client = new pg.Client({
    database: env.DB_NAME,
    user: env.DB_USERNAME,
    password: env.DB_PASSWORD,
});
export interface User {
    username: string;
    password: string;
}
export interface Memo {
    content: string;
    image?: string;
}
async function main() {
    try {
        await client.connect(); // "dial-in" to the postgres server
        let workbook = XLSX.readFile("data.xlsx");
        let userWs = workbook.Sheets["user"];
        let memoWs = workbook.Sheets["memo"];

        let users: User[] = XLSX.utils.sheet_to_json(userWs);
        let memos: Memo[] = XLSX.utils.sheet_to_json(memoWs);
        await client.query("truncate table users RESTART IDENTITY;");
        await client.query("truncate table memos RESTART IDENTITY;");
        for (let user of users) {
            // await client.query(
            //     `INSERT INTO users (username,password, create_at, update_at)  values ('${user.username}',${user.password}, current_timestamp, current_timestamp)`
            // );

            let hashedPassword = await hashPassword(user.password)

            await client.query(
                "INSERT INTO users (username,password, create_at, update_at) values ($1,$2, current_timestamp, current_timestamp)",
                [user.username, hashedPassword]
            );
        }
        await client.query("truncate table memos RESTART IDENTITY;");
        for (let memo of memos) {
            await client.query(
                "INSERT INTO memos (content,image, create_at, update_at) values ($1,$2, current_timestamp, current_timestamp)",
                [memo.content, memo.image]
            );
        }
    } catch (error) {
        console.log(error);
    } finally {
        await client.end(); // close connection with the database
    }
}
main();
