"use strict";
// Declaration of Class and its methods
var Player = /** @class */ (function () {
    function Player(strength, name) {
        this.strength = strength;
        this.name = name;
    }
    Player.prototype.attack = function (monster) {
        var exp = monster.hp;
        while (monster.hp > 0) {
            if (Math.random() < 1 / 4) {
                monster.injure(this.strength * 2);
                console.log("Player " + this.name + " attacks a monster (HP: " + monster.hp + ") [CRITICAL]");
            }
            else {
                monster.injure(this.strength);
                console.log("Player " + this.name + " attacks a monster (HP: " + monster.hp + ")");
            }
        }
        this.gainExperience(exp);
    };
    Player.prototype.gainExperience = function (exp) {
        console.log("Player " + this.name + " gain " + exp + " exp");
    };
    return Player;
}());
var Monster = /** @class */ (function () {
    // Think of how to write injure
    function Monster(hp) {
        if (hp === void 0) { hp = 100; }
        this.hp = hp;
    }
    Monster.prototype.injure = function (strength) {
        this.hp -= strength;
    };
    return Monster;
}());
// Invocations of the class and its methods
var player = new Player(21, 'Peter');
var monster = new Monster();
player.attack(monster);
// English counterpart: Player attacks monster
setTimeout(function () { }, 1000 * 1000);
//# sourceMappingURL=ex1_ans.js.map