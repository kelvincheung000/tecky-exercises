// // Declaration of Class and its methods
// class Player {
//     private strength: number;
//     private name: string;
//     constructor(strength: number, name: string) {
//         this.strength = strength;
//         this.name = name;
//     }

//     attack(monster: Monster) {
//         monster.injure(this.strength)
//         if (monster.getHp() > 0) {
//             console.log(`Player ${this.name} attacks a monster (HP: ${monster.getHp()})`)
//         } else {
//             console.log(`Player ${this.name} attacks a monster (HP: 0)`)
//         }
//     }

//     gainExperience(exp: number) {
//         this.gainExperience // not sure
//     }
// }

// class Monster {
//     private hp: number;
//     constructor(hp: number) {
//         this.hp = hp;
//     }

//     injure(strength: number) {
//         this.hp -= strength;
//         if (this.hp <= 0) {
//             this.hp = 0;
//         }
//     }
//     getHp() {
//         return this.hp;
//     }
// }

// // Invocations of the class and its methods
// const player = new Player(20, 'Peter');
// const monster = new Monster(100);
// player.attack(monster);
// // English counterpart: Player attacks monster