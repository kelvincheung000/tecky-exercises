import { cyan, reset, green } from 'asciichart'

// Declaration of Class and its methods
class Player {
    private strength: number;
    private name: string;
    private criticalRate = 1 / 4;

    constructor(strength: number, name: string) {
        this.strength = strength;
        this.name = name;
    }

    attack(monster: Monster) {
        let exp = monster.hp;
        while (monster.hp > 0) {
            if (Math.random() < this.criticalRate) {
                monster.injure(this.strength * 2);
                console.log(`${green}Player${reset} ${this.name} attacks a monster (HP: ${monster.hp}) ${cyan}[CRITICAL]${reset}`)
            } else {
                monster.injure(this.strength)
                console.log(`${green}Player${reset} ${this.name} attacks a monster (HP: ${monster.hp})`)
            }
        }
        this.gainExperience(exp)
    }

    gainExperience(exp: number) {
        console.log(`Player ${this.name} gain ${exp} exp`)
    }

}

class Monster {
    // Think of how to write injure
    constructor(public hp: number = 100) { }

    getHP() {
        return this.hp
    }

    injure(strength: number) {
        if (this.hp >= strength) {
            this.hp -= strength;
        } else {
            this.hp = 0
        }
    }
}

// Invocations of the class and its methods
const player = new Player(20, 'Peter');
const monster = new Monster(100);
player.attack(monster);
// English counterpart: Player attacks monster

setTimeout(() => { }, 1000 * 1000)