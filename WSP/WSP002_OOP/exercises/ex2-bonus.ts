import { cyan, reset, green } from 'asciichart'

interface Attack {
    damage: number
}

class BowAndArrow implements Attack {
    //Bow and Arrow Attack here
    constructor(public damage: number) { }
}

class ThrowingSpear implements Attack {
    // Throwing Spear Attack here
    constructor(public damage: number) { }
}

class Swords implements Attack {
    constructor(public damage: number) { }
}

class MagicSpells implements Attack {
    constructor(public damage: number) { }
}

interface IPlayer {
    attack(monster: Monster): void
    switchAttack(): void
    gainExperience(exp: number): void
}

class Player implements IPlayer {
    private primary: Attack
    private secondary: Attack
    private nextAttack: Attack
    private type: string

    constructor(type: string) {
        let config = configs.find(config => config.name == type)
        if (!config) { throw new Error('unknown player type: ' + type) }
        this.type = type
        this.primary = config.primary
        this.secondary = config.secondary
        this.nextAttack = this.primary
    }

    attack(monster: Monster): void {
        let exp = monster.getHP()
        while (monster.getHP() > 0) {
            let attackName = this.nextAttack.constructor.name
            let hasCritical = false;
            if (Math.random() < 1 / 4) {
                monster.injure(this.nextAttack.damage)
                hasCritical = true
            } else {
                monster.injure(this.nextAttack.damage)
            }
            let msg = `attacks a monster with ${attackName} (HP: ${monster.getHP()})`
            if (hasCritical) {
                msg += ` ${cyan}[CRITICAL]${reset}`
            }
            this.report(msg)
            this.switchAttack()
        }
        this.gainExperience(exp)
    }

    switchAttack() {
        if (this.nextAttack == this.primary) {
            this.nextAttack = this.secondary
        } else {
            this.nextAttack = this.primary
        }
    }

    //.. The remaining methods.
    gainExperience(exp: number): void {
        this.report(`gains ${exp} exp`)
    }


    report(message: string) {
        // let name = this.constructor.name
        let name = this.type
        console.log(green, name, reset, message)
    }
}

interface PlayerConfig {
    name: string
    primary: Attack
    secondary: Attack
}

let configs: PlayerConfig[] = []
configs.push({
    name: 'Amazon',
    primary: new BowAndArrow(30),
    secondary: new ThrowingSpear(40)
})
configs.push({
    name: 'Paladin',
    primary: new Swords(50),
    secondary: new MagicSpells(25)
})
configs.push({
    name: 'Barbarian',
    primary: new Swords(55),
    secondary: new ThrowingSpear(30)
})

class Monster {
    constructor(private hp: number = 100) { }

    getHP() {
        return this.hp
    }

    injure(strength: number) {
        if (this.hp >= strength) {
            this.hp -= strength
        } else {
            this.hp = 0
        }
    }
}

let amazonPlayer = new Player('Amazon')
let monster1 = new Monster()
amazonPlayer.attack(monster1)

let paladinPlayer = new Player('Paladin')
let monster2 = new Monster()
paladinPlayer.attack(monster2)

let barbarianPlayer = new Player('Barbarian')
let monster3 = new Monster(1001)
barbarianPlayer.attack(monster3)

setTimeout(() => { }, 1000 * 1000)
