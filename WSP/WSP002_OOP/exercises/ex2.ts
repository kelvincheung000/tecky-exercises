import { cyan, reset, green } from 'asciichart'

interface Attack {
    damage: number
}

class BowAndArrow implements Attack {
    //Bow and Arrow Attack here
    constructor(public damage: number) { }
}

class ThrowingSpear implements Attack {
    // Throwing Spear Attack here
    constructor(public damage: number) { }
}

class Swords implements Attack {
    constructor(public damage: number) { }
}

class MagicSpells implements Attack {
    constructor(public damage: number) { }
}

interface Player {
    attack(monster: Monster): void
    switchAttack(): void
    gainExperience(exp: number): void
}

abstract class BasePlayer implements Player {
    protected abstract primary: Attack
    protected abstract secondary: Attack
    private usePrimaryAttack: boolean

    constructor() {
        this.usePrimaryAttack = true
    }

    attack(monster: Monster): void {
        let exp = monster.getHP()
        while (monster.getHP() > 0) {
            if (this.usePrimaryAttack) {
                monster.injure(this.primary.damage)
                this.report(
                    `attacks a monster (HP: ${monster.getHP()}) ${cyan}[CRITICAL]${reset}`,
                )
            } else {
                monster.injure(this.secondary.damage)
                this.report(`attacks a monster (HP: ${monster.getHP()})`)
            }
            this.switchAttack()
        }
        this.gainExperience(exp)
    }

    switchAttack() {
        // TODO: Change the attack mode for this player
        this.usePrimaryAttack = !this.usePrimaryAttack
    }

    //.. The remaining methods.
    gainExperience(exp: number): void {
        this.report(`gains ${exp} exp`)
    }

    report(message: string) {
        let name = this.constructor.name
        console.log(green, name, reset, message)
    }
}

class Amazon extends BasePlayer {
    primary = new BowAndArrow(30)
    secondary = new ThrowingSpear(40)
}
class Paladin extends BasePlayer {
    primary = new Swords(50)
    secondary = new MagicSpells(25)
}
class Monster {
    constructor(private hp: number = 100) { }

    getHP() {
        return this.hp
    }

    injure(strength: number) {
        if (this.hp >= strength) {
            this.hp -= strength
        } else {
            this.hp = 0
        }
    }
}

let amazonPlayer = new Amazon()
let monster1 = new Monster()
amazonPlayer.attack(monster1)

let paladinPlayer = new Paladin()
let monster2 = new Monster()
paladinPlayer.attack(monster2)

setTimeout(() => { }, 1000 * 1000)
