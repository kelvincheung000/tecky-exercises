abstract class Animal {
    constructor(public name: string) { }
    abstract eat(): void;
    abstract sleep(): void;
}

class Human extends Animal {
    constructor(public name: string) {
        super(name);
    }

    eat(): void {
        // some eating logic
    }

    sleep(): void {
        // some sleeping logic
    }
}
// const animal = new Animal();

// const human = new Human();