// const bob:Student = new Student()


class Student {
    name: string;
    age: number;
    learningLevel: number;

    // Constructor
    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
        this.learningLevel = this.learningLevel;
    }
    // Methods
    learn(hourSpent: number) {
        this.learningLevel += hourSpent * 0.3;
        this.slack(hourSpent / 5);
    }
    slack(hourSpent: number) {
        this.learningLevel -= hourSpent * 0.1;
    }
}

// NOTES:
// Always name our Class after Pascal Case which we also have the first letter to be capitalized.
// Constructor - The first method that is being called when the instance is instantiated.
// Fields - The attributes that can be performed by the instances.
// Methods - The actions that can be performed by the instances.
// The name and the age are the initial values that are provided in the parameters.
// The value learningLevel is the initial value of any Student initialized.
// The "this" preceding each property
// The "this" there represents the current object


// e.g.
// const student = new Student("Tom", 25);
// const student2 = new Student("Ben", 35);

// const ben = new Student("Ben", 29);
// ben.learn(3);
// ben.slack(1);
// ben.learn(2);
// ben.slack(3);
// console.log(ben.learningLevel)


class CodingStudent extends Student {
    constructor(name: string, age: number) {
        super(name, age); // constructor Student(name: string, age: number): Student
        this.learningLevel = 10;
    }
    learn(hourSpent: number) {
        this.learningLevel += hourSpent * 0.5;
    }
    slack(hourSpent: number) {
        super.slack(hourSpent);  // class Student
        this.learningLevel -= hourSpent * 0.3;
    }
    readReddit(hourSpent: number) {
        this.learningLevel += hourSpent * 0.2;
    }
}


// CodingStudent is a type of Student because coding students are also students. 
// We normally say CodingStudent is a subtype of Student.
// when we are initializing a subclass, we always have to initialize the superclass first.
// need to call the superclass constructor super(name,age) which is essentially calling 
// the constructor of Student. If we don't follow this rule, there will be an error of this.
// a subclass like CodingStudent can override the method from the superclass by 
// defining the new version of the methods learn and slack.

class TeckyStudent extends CodingStudent {
    slack(hourSpent: number) {
        this.learningLevel -= hourSpent * 0.8;
    }
}

// const tony = new TeckyStudent(age);


