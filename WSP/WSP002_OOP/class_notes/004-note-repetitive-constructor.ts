// Full version of Typescript class
class Person {

    private name: string;
    private age: number;
    private email: string;
    constructor(name: string, age: number, email: string) {
        this.name = name;
        this.age = age;
        this.email = email;
    }
}

// Short form version of Typescript class
class Person {
    constructor(private name: string,
        private age: number,
        private email: string) { }
}

// Javascript class
class Person {
    constructor(name, age, email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }
}