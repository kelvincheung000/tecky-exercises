class Person {
    protected name: string;
    constructor(name: string) {
        this.name = name;
    }
}

class Student extends Person {
    private currentClass: string;

    constructor(name: string, currentClass: string) {
        super(name);
        this.name = `Student ${this.name}`;
        this.currentClass = currentClass;
    }
}
const s = new Student("John", "F.1");
s.name = "Peter";