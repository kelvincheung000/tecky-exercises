interface FlyingAnimal {
    // method:
    fly(height: number): void;
}

interface Mammal {
    breastFeed(): void;
}

interface Bird {
    layEggs(numberOfEggs: number): void;
}

// NOTES:
// uses the keyword implements for a specifying interface
// Interfaces help developers to specify the behaviours of a type 
// without actually specifying the internal implementations. 

class Eagle implements FlyingAnimal {
    fly(height: number): void {
        console.log(`Eagle flies to ${height} using hot air stream`);
    }
    layEggs(numberOfEggs: number) {
        console.log(`Eagle lay ${numberOfEggs} eggs in nest`);
    }
}
class Bat implements FlyingAnimal {
    fly(height: number): void {
        console.log(`Bat flies to ${height} using ultrasonic`);
    }
    breastFeed() {
        console.log("Bat breastfeed its babies")
    }
}
class Dragonfly implements FlyingAnimal {
    fly(height: number): void {
        console.log(`Dragonfly flies to ${height} using its insect wings`);
    }
}

function flyingContest(flyingAnimals: FlyingAnimal[]) {
    for (let flyingAnimal of flyingAnimals) {
        flyingAnimal.fly(100);
    }
}
// flyingContest([eagle, bat, dragonfly]);


