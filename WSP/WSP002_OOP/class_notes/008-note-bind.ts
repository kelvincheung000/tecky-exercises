function deposit(this: any, amount: number) {
    this.total += amount;
    console.log(`Amount ${amount} is deposited to the account of ${this.name}`);
    return this.total;
}

const gordonAcc = { total: 3000, name: 'gordon' };

const depositToGordonAcc = deposit.bind(gordonAcc)
depositToGordonAcc(100);
console.log(gordonAcc);

> Amount 100 is deposited to the account of gordon
    > { total: 3100, name: "gordon" }