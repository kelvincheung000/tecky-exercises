
class Person {
    public name: string;
    constructor(name: string) {
        this.name = name;
    }
}

const p = new Person("John");
p.name = "Peter";
console.log(p.name); // Print Peter


class Person2 {
    private name: string;
    constructor(name: string) {
        this.name = name;
    }
}

const q = new Person("John");
q.name = "Peter"; // ERROR here!
// Private attributes are not accessible outside the current class and any subclass.