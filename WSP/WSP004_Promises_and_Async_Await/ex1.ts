
import fs from "fs"
import path from "path"

function fsReadDirectoryPromise(filePath: string) {
    return new Promise(function (resolve, reject) {
        fs.readdir(filePath, (err, files) => {
            if (err) {
                reject(err);
            }
            resolve(files);
        })
    })
}

function fsStatPromise(filePath: string) {
    return new Promise(function (resolve, reject) {
        fs.stat(filePath, (err, stats) => {
            if (err) {
                reject(err);
            }
            resolve(stats);
        })
    })
}

async function listAllJsRecursive(folderPath: string){
    let files = await fsReadDirectoryPromise(folderPath); // files is now array of filenames.
     // rest of the code.
}



// function listAllJsRecursive(dir: string) {
//     fs.readdir(dir, (err, files) => {
//         if (err) {
//             console.error('Failed to read dir:', err)
//             return
//         }
//         for (let filename of files) {
//             let file = path.join(dir, filename)  // why this time place here as compared with ex1?
//             fs.stat(file, (err, stats) => {
//                 if (stats.isFile()) {
//                     let ext = path.extname(filename)
//                     if (ext === '.js') {
//                         console.log(file)
//                     }
//                     return  // ends function execution here??
//                 }
//                 if (stats.isDirectory()) {
//                     listAllJsRecursive(file)
//                     return  // ends function execution here??
//                 }
//             })
//         }
//     })
// }

// listAllJsRecursive('/Users/kelvin_cheung/Documents/Tecky_Academy/class_notes_and_exercises/tecky-exercises-solutions-sw')

