import fs, { readdir } from "fs"
// import path, { resolve } from "path"
// import util from 'util'

export let pfs = {
    readFile: (file: string): Promise<Buffer> =>
        new Promise((resolve, reject) =>
            fs.readFile(file, (error, data) => {
                if (error) {
                    reject(error)
                } else
                    resolve(data)
            })
        )
        readdir: (dir: string): Promise<string[]> =>
        new Promise((resolve, reject) =>
            fs.readdir(dir, (error, files) =>
                error ? reject(error) : resolve(files)
            ))
}




// function listAllJsRecursive(dir: string) {
//     fs.readdir(dir, (err, files) => {
//         if (err) {
//             console.error('Failed to read dir:', err)
//             return
//         }
//         for (let filename of files) {
//             let file = path.join(dir, filename)  // why this time place here as compared with ex1?
//             fs.stat(file, (err, stats) => {
//                 if (stats.isFile()) {
//                     let ext = path.extname(filename)
//                     if (ext === '.js') {
//                         console.log(file)
//                     }
//                     return  // ends function execution here??
//                 }
//                 if (stats.isDirectory()) {
//                     listAllJsRecursive(file)
//                     return  // ends function execution here??
//                 }
//             })
//         }
//     })
// }

// listAllJsRecursive('/Users/kelvin_cheung/Documents/Tecky_Academy/class_notes_and_exercises/tecky-exercises-solutions-sw')

