import readline from 'readline'

let io = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

io.question('What is your name? ', answer => {
    clearInterval(timer)
    console.log('Hi,', answer)
    io.close()
})

let timer = setInterval(() => {
    process.stdout.write('.')
}, 1000)

