import path from 'path'
import fs from 'fs'

let file = path.join('data', 'counter.txt')
console.log(file)

fs.readFile(file, (err, data) => {
    if (err) {
        console.error('Failed to read counter.txt:', err)
        process.exit(1)
    }
    let text = data.toString()
    let counter = +text
    console.log({ counter })

    counter++
    text = counter.toString()
    fs.writeFile(file, text, err => {
        if (err) {
            console.error('Failed to save context.txt:', err)
        }
    })

    counter++
    console.log({ counter })

    console.log('do other works...')
})
