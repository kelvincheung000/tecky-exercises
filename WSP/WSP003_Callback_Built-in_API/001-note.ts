import fs from 'fs'

let start = Date.now()

let jobs = 0

fs.readFile('package.json', (err, data) => {
    let end = Date.now()
    let used = end - start
    jobs++
    console.log('read package.json result:', {
        err,
        data: data.length,
        used,
        jobs,
    })
})



fs.readFile('01-fs.ts', (err, data) => {
    let end = Date.now()
    let used = end - start
    jobs++
    console.log('read 01-fs.ts result:', { err, data: data.length, used, jobs })
})