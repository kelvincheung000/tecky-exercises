let start = Date.now()


console.log('task 1')
setTimeout(() => {
    console.log('timeout 1:', Date.now() - start)
}, 1000)

console.log('task 2')
setTimeout(() => {
    console.log('timeout 2:', Date.now() - start)
}, 2000)

console.log('task 3')
setTimeout(() => {
    console.log('timeout 3:', Date.now() - start)
}, 1000)

console.log('task 4')
setImmediate(() => {
    console.log('immediate 4:', Date.now() - start)
})

console.log('task 5')
setTimeout(() => {
    console.log('timeout 5:', Date.now() - start)
}, 0)

console.log('task 6')
setImmediate(() => {
    console.log('immediate 6:', Date.now() - start)
})