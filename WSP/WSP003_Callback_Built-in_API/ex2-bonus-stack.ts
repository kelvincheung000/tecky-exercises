import fs from 'fs'
import path from 'path'

interface Task {
    run(cb: (error: Error | null) => void): void
}

interface JsFileStack {
    addTask(task: Task): void
    addJsFile(file: string): void
    whenEmpty(cb: (error: Error | null, jsFiles: string[]) => void): void
}

interface JsFileCallback {
    (error: Error | null, jsFiles: string[]): void
}

// type JsFileCallback = (error: Error | null, jsFiles: string[]) => void

class SetStack implements JsFileStack {
    totalTaskNumber = 0
    pendingTasks = new Set<Task>()
    matchedFiles: string[] = []
    error: Error | null = null
    doneCallback?: JsFileCallback

    lastProgressMessageLength = 0

    report() {
        let done = this.totalTaskNumber - this.pendingTasks.size
        let msg = `\r  progress: ${done}/${this.totalTaskNumber}`
        this.lastProgressMessageLength = msg.length
        process.stderr.write(msg)
    }

    addTask(task: Task): void {
        if (this.error) {
            return
        }
        this.totalTaskNumber++
        this.pendingTasks.add(task)
        task.run(error => {
            if (error) {
                this.error = error
            }
            this.pendingTasks.delete(task)
            this.checkProgress()
        })
    }

    checkProgress() {
        this.report()
        if (this.pendingTasks.size === 0) {
            process.stderr.write(`\r`)
            process.stderr.write(' '.repeat(this.lastProgressMessageLength))
            process.stderr.write(`\r`)
            this.doneCallback?.(this.error, this.matchedFiles.sort())
        }
    }

    addJsFile(file: string): void {
        this.matchedFiles.push(file)
    }

    whenEmpty(cb: JsFileCallback): void {
        this.doneCallback = cb
        this.checkProgress()
    }
}

class ScanFileTask implements Task {
    constructor(
        public stack: JsFileStack,
        public dir: string,
        public filename: string,
        public ext: string,
    ) { }
    run(cb: (error: Error | null) => void): void {
        let file = path.join(this.dir, this.filename)
        fs.stat(file, (error, stats) => {
            if (!error) {
                if (stats.isDirectory()) {
                    this.stack.addTask(new ScanDirTask(this.stack, file, this.ext))
                } else if (stats.isFile()) {
                    let ext = path.extname(this.filename)
                    if (ext === this.ext) {
                        this.stack.addJsFile(file)
                    }
                }
            }
            cb(error)
        })
    }
}

// class ScanJsFileTask extends ScanFileTask {
//   ext = '.js'
// }

// class ScanTsFileTask extends ScanFileTask {
//   ext = '.ts'
// }

class ScanDirTask implements Task {
    constructor(
        public stack: JsFileStack,
        public dir: string,
        public ext: string,
    ) { }
    run(cb: (error: Error | null) => void): void {
        fs.readdir(this.dir, (error, files) => {
            if (!error) {
                files.forEach(filename => {
                    this.stack.addTask(
                        new ScanFileTask(this.stack, this.dir, filename, this.ext),
                    )
                })
            }
            cb(error)
        })
    }
}

function listAllFileRecursive(
    dir: string,
    ext: string,
    cb: (error: Error | null, jsFiles: string[]) => void,
) {
    let stack = new SetStack()
    stack.addTask(new ScanDirTask(stack, dir, ext))
    stack.whenEmpty(cb)
}

function listAllJsRecursive(
    dir: string,
    cb: (error: Error | null, jsFiles: string[]) => void,
) {
    return listAllFileRecursive(dir, '.js', cb)
}

let entryDir = 'node_modules'
entryDir =
    '/Users/kelvin_cheung/Documents/Tecky_Academy/class_notes_and_exercises/tecky-exercises-solutions-sw'
// entryDir = 'node_modules/ts-node/dist'

listAllJsRecursive(entryDir, (error, jsFiles) => {
    console.log('Error:', error)
    console.log('jsFiles:', jsFiles.length)
})

listAllFileRecursive(entryDir, '.ts', (error, files) => {
    console.log('Error:', error)
    console.log('tsFiles:', files.length)
})

listAllFileRecursive(entryDir, '.json', (error, files) => {
    console.log('Error:', error)
    console.log('json files:', files)
})
