

import fs from 'fs'
import path from 'path'

function listAllJsRecursive(dir: string) {
    fs.readdir(dir, (err, files) => {
        if (err) {
            console.log('Failed to read dir:', err)
            return
        }
        for (let filename of files) {
            let file = path.join(dir, filename);
            fs.stat(file, (err, stats) => {
                if (stats.isFile()) {
                    let ext = path.extname(filename)
                    if (ext === '.js') {
                        console.log(file)
                    }
                    return
                }
                if (stats.isDirectory()) {
                    listAllJsRecursive(file)
                    return
                }
            })
        }
    })
}

listAllJsRecursive('/Users/kelvin_cheung/Documents/Tecky_Academy/class_notes_and_exercises/tecky-exercises-solutions-sw')