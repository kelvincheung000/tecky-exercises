import path from 'path'
import fs from 'fs'

function checkIn() {
    let file = path.join('data', 'log.txt')
    let text = new Date().toString() + '\n'
    fs.writeFile(file, text, { flag: 'a' }, (err) => {
        if (err) {
            console.error('failed to write log.txt ' + err)
        } else {
            console.log('appended to log.txt')
        }
    })
}

checkIn()
setTimeout(checkIn, 1000)
setTimeout(checkIn, 2000)

setTimeout(() => {
    checkIn()
    setTimeout(() => {
        checkIn()
    }, 2000)
}, 1000)
