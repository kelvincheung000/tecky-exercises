
function compute(name: string, n: number) {
    console.log('start:', { name })
    let start = Date.now()
    for (let i = 0; i < n; i++) { }  // what's the purpose of adding for loop here?
    let end = Date.now()
    let used = end - start
    let result = {
        name,
        start: new Date(start).toLocaleTimeString(),
        end: new Date(end).toLocaleTimeString(),
        used,
    }
    console.log('finish:', result)
    return result
}

let unit = 1_000_000_000

interface Task {
    name: string
    n: number

    // 0 most important, 1 less important, 2 even less important
    priority: number

    next?: Task  // optional
}

class Queue {
    tasks: Task[] = []
    enqueue(task: Task) {
        this.tasks.push(task)
    }
    compute() {
        this.tasks.sort((a, b) => b.priority - a.priority)
        let task = this.tasks.pop()
        if (!task) return
        compute(task.name, task.n)
        if (task.next) {
            this.enqueue(task.next)
        }
        this.compute()
    }
}

let queue = new Queue()

let flow: Task = {
    name: 'cut apple',
    n: 0.5 * unit,
    priority: 0,
    next: {
        name: 'make apple juice',
        n: 1 * unit,
        priority: 2,
        next: {
            name: 'package apple juice',
            n: 0.8 * unit,
            priority: 1,
            next: {
                name: 'sell apple juice',
                n: 2 * unit,
                priority: 0,
            }
        }
    }
}

queue.enqueue(flow)
queue.enqueue(flow)
queue.enqueue(flow)
queue.enqueue(flow)

queue.compute()