import fs from 'fs'
import path from 'path'


function listAllJsRecursive(
    dir: string,
    cb: (error: Error | null, jsFiles: string[]) => void,
) {
    fs.readdir(dir, (error, files) => {
        if (error) {
            cb(error, [])
            return
        }
        if (files.length == 0) {
            cb(null, [])
            return
        }
        let jsFiles: string[] = []
        let fileDone = 0
        for (let filename of files) {
            let file = path.join(dir, filename);
            fs.stat(file, (error, stats) => {
                if (error) {
                    cb(error, jsFiles)
                    fileDone++
                    return
                }
                if (stats.isFile()) {
                    let ext = path.extname(filename)
                    if (ext === '.js') {
                        jsFiles.push(file)
                    }
                    fileDone++
                    if (fileDone == files.length) {
                        cb(null, jsFiles)
                    }
                    return
                }
                if (stats.isDirectory()) {
                    listAllJsRecursive(file, (error, jsFilesFromChild) => {
                        if (error) {
                            cb(error, jsFiles)
                            fileDone++
                            return
                        }
                        // for (let file of jsFilesFromChild) {
                        //     jsFiles.push(file)
                        // }
                        jsFiles.push(...jsFilesFromChild) // 自動將入邊內容逐個push入file到
                        fileDone++
                        if (fileDone == files.length) {
                            cb(null, jsFiles)
                        }
                    })
                    return
                }
            })
        }
    })
}

let entryDir = 'node_modules'
entryDir =
    '/Users/kelvin_cheung/Documents/Tecky_Academy/class_notes_and_exercises/tecky-exercises-solutions-sw'
// entryDir = 'node_modules/ts-node/dist'

listAllJsRecursive(entryDir, (error, jsFiles) => {
    console.log('Error:', error)
    console.log('jsFiles:', jsFiles.length)
})

