import readline from 'readline'

let io = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

let min = 1
let max = 100
let answer = 42

function loop() {
    io.question(`Guess a number (${min} - ${max}): `, input => {
        let guess = +input
        if (guess < answer) {
            console.log('too small')
            min = guess + 1
        } else if (guess > answer) {
            console.log('too large')
            max = guess - 1
        } else if (guess == answer) {
            console.log('Congratulation')
            io.close()
            return
        } else {
            console.log('invalid input')
        }
        loop()
    })
}


loop()
