import fs from 'fs'
import path from 'path'

fs.readdir('node_modules', (err, files) => {
    console.log({ err, files })
    for (let filename of files) {
        let file = path.join('node_modules', filename)
        fs.stat(file, (err, stats) => {  // use a call back to return the results
            if (stats.isDirectory()) {
                let dir = file
                fs.readdir(dir, (err, files) => {
                    for (let filename of files) {
                        let file = path.join(dir, filename)
                        console.log(file)
                    }
                })
            } else if (stats.isFile()) {
                console.log('file', file)
            }
        })
    }
})




// NOTES:
// fs.readdir(): read the contents of a directory
// fs.stat(): returns the status of the file identified by the filename passed. 
// fs.readdir(): read the contents of a directory
// The path.join() method joins the specified path segments into one path.
