// Exercise 1: Directory Listing

import fs from 'fs'
import path from 'path'

function listAllJs(dir: string) {  // why put directory as parameter here?
    fs.readdir(dir, (err, files) => {
        if (err) {
            console.log('Failed to read dir:', err)
            return
        }
        for (let filename of files) {
            let ext = path.extname(filename); // why let ext inside for loop but not the outside of for loop?
            if (ext === '.js') {
                let file = path.join(dir, filename);
                console.log(file);
            }
        }
    })
}

listAllJs('node_modules/ts-node/dist')


// fs.readdir() method is used to asynchronously read the contents of a given directory.
// The path.extname() method returns the extension of a file path.
// The path.join() method joins the specified path segments into one path.