// Exercise 2: Directory Traversal

import fs from "fs"
import path from "path";

function listAllJsRecursive(dir: string) {
    fs.readdir(dir, (err, files) => {
        if (err) {
            console.error('Failed to read dir:', err)
            return
        }
        for (let filename of files) {
            let file = path.join(dir, filename)  // why this time place here as compared with ex1?
            fs.stat(file, (err, stats) => {
                if (stats.isFile()) {
                    let ext = path.extname(filename)
                    if (ext === '.js') {
                        console.log(file)
                    }
                    return
                }
                if (stats.isDirectory()) {
                    listAllJsRecursive(file)
                    return
                }
            })
        }
    })
}

listAllJsRecursive('/Users/kelvin_cheung/Documents/Tecky_Academy/class_notes_and_exercises/tecky-exercises-solutions-sw')


// stats.isFile() 
// an inbuilt application programming interface of the fs.Stats class 
// which is used to check whether fs.Stats object describes a file or not.

// stats.isDirectory()
// an inbuilt application programming interface of the fs.Stats class 
// which is used to check whether fs.Stats object describes a file system directory or not.