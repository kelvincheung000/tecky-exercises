import express from 'express'
import { loadDataFile } from './data'
import { red, reset } from 'asciichart'

export let userRoutes = express.Router()

userRoutes.use(express.urlencoded({ extended: false }))

export type User = {
  username: string
  password: string
}

let users = loadDataFile<User[]>('users.json', [])
if (users.data.length == 0) {
  console.error(red + 'Error: missing users in users.json' + reset)
  process.exit(1)
}

userRoutes.post('/login', (req, res) => {
  let { username, password } = req.body
  if (!username) {
    res.status(400).json({ error: 'missing username' })
    return
  }
  if (!password) {
    res.status(400).json({ error: 'missing password' })
    return
  }
  let user = users.data.find(user => user.username === username)
  if (!user) {
    res.status(404).json({ error: 'user not found' })
    return
  }
  if (user.password !== password) {
    res.status(403).json({ error: 'wrong password' })
    return
  }
  req.session.username = username
  res.json({
      message: "login successfully",
  });
  // res.redirect('/')
})

userRoutes.get('/me', (req, res) => {
  let currentUser = req.session.username ? req.session.username : "NA"
  res.json({
      user: currentUser,
  });
})
userRoutes.post('/logout', (req, res) => {
  req.session.destroy(error => {
    if (error) {
      console.error('failed to destroy session:', error)
    }
    res.redirect('/')
  })
})
