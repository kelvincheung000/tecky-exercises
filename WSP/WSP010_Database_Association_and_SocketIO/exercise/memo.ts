import express from 'express'
import formidable from 'formidable'
import { mkdirSync } from 'fs'
import { loadDataFile } from './data'
import { client } from './server'

export let memoRoutes = express.Router()

let uploadDir = 'uploads'

mkdirSync(uploadDir, { recursive: true })

let form = formidable({
  uploadDir,
  allowEmptyFiles: true,
  maxFiles: 1,
  maxFileSize: 200 * 1024 ** 2,
  keepExtensions: true,
  filter: part => part.mimetype?.startsWith('image/') || false,
})

export type Memo = {
  id: number
  content: string
  image?: string
}
let memos = loadDataFile<Memo[]>('memo.json', [
  { id: 1, content: '網上連儂牆' },
  { id: 2, content: '香港加油' },
])
let nextMemoId = 1
for (let memo of memos.data) {
  nextMemoId = Math.max(nextMemoId, memo.id + 1)
}

memoRoutes.post('/memos', (req, res) => {
  form.parse(req, (err, fields, files) => {
    console.log('post memo:', { err, fields, files })
    if (err) {
      res.status(400).json({ error: String(err) })
      return
    }
    let content = fields.content
    if (typeof content !== 'string' || !content) {
      res
        .status(400)
        .json({ error: 'invalid content, expect an non-empty string' })
      return
    }
    console.log('post memo ,', content);

    let file = Array.isArray(files.image) ? files.image[0] : files.image
    let image = file ? file.newFilename : undefined
    memos.data.push({
      id: nextMemoId,
      content,
      image,
    })
    nextMemoId++
    memos.save()
    res.json({
      message: "Add memo success"
    })
    // res.redirect('/')
  })
})


memoRoutes.get('/memos', async (req, res) => {
  let memoRes = await client.query(`select * from memos`)
  let memoData = memoRes.rows;
  // console.log(memoRes)
  // let memoList = memos.data.sort((a, b) => b.id - a.id);
  res.json(memoData);
})

memoRoutes.patch("/memos", (req, res) => {
  let { id, content } = req.body
  let memoId = Number(id)
  console.log({ id, content });

  let updatedMemos = memos.data.map((memoItem) => {
    if (memoItem.id === memoId) {
      return {
        id: memoId,
        content,
        image: memoItem.image
      };
    } else {
      return memoItem;
    }
  })

  memos.data = updatedMemos;
  memos.save()

  res.json({
    message: 'updated success'
  });
});
memoRoutes.get('/memos.js', (req, res) => {
  let memoList = memos.data.sort((a, b) => b.id - a.id)
  res.setHeader('Content-Type', 'application/javascript')
  res.end(`let memos = ${JSON.stringify(memoList)}`)
})

memoRoutes.use('/uploads', express.static(uploadDir))
