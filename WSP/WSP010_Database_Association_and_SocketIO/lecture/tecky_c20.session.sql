CREATE TABLE teachers(
    id SERIAL PRIMARY KEY,
    name varchar(255),
    date_of_birth DATE
);
CREATE TABLE students(
    id SERIAL PRIMARY KEY,
    SELECT *
    FROM students
        join teachers on teachers.id = students.teacher_id;
insert into students (name, level, date_of_birth, teacher_id)
VALUES (
        'Peter',
        25,
        '1995-05-15',
        (
            SELECT id
            from teachers
            where name = 'Bob'
        )
    ),
    (
        'John',
        25,
        '1985-06-16',
        (
            SELECT id
            from teachers
            where name = 'Bob'
        )
    ),
    ('Simon', 25, '1987-07-17', NULL);
name varchar(255),
level integer,
date_of_birth DATE,
teacher_id integer,
FOREIGN KEY (teacher_id) REFERENCES teachers(id)
);
insert into teachers(name, date_of_birth)
VALUES ('Bob', '1970-01-01');
insert into students (name, level, date_of_birth, teacher_id)
VALUES (
        'Peter',
        25,
        '1995-05-15',
        (
            SELECT id
            from teachers
            where name = 'Bob'
        )
    ),
    (
        'John',
        25,
        '1985-06-16',
        (
            SELECT id
            from teachers
            where name = 'Bob'
        )
    ),
    ('Simon', 25, '1987-07-17', NULL);
SELECT *
FROM students
    join teachers on teachers.id = students.teacher_id;