import { checkFizzBuzz, fizzBuzz } from "./fizzbuss";

xdescribe("testing on fizzBuzz()", () => {
    it("can handle positive case ", () => {
        // Positive
        expect(fizzBuzz(1)).toBe("1");
        expect(fizzBuzz(2)).toBe("2");
        expect(fizzBuzz(3)).toBe("Fizz");
        expect(fizzBuzz(4)).toBe("4");
        expect(fizzBuzz(5)).toBe("Buzz");
        expect(fizzBuzz(15)).toBe("Fizz Buzz");
    });

    it("to handle negative case ", () => {
        // Negative
        expect(() => {
            fizzBuzz(0)
        }).toThrow("Input should not be negative");
    });
});

describe("testing on checkFizzBuzz()", () => {
    it("can return true when ans is correct in time", () => {
        expect(checkFizzBuzz(15, "Fizz Buzz", 1.3)).toBeTruthy();
        expect(checkFizzBuzz(3, "Fizz", 0.3)).toBeTruthy();
    });
    it("can return false when ans is incorrect", () => {
        expect(checkFizzBuzz(14, "FXXK", 1.3)).not.toBeTruthy();
    });
    it("can return false when player think too long", () => {
        expect(checkFizzBuzz(15, "Fizz Buzz", 2000)).toBeFalsy();
    });
});























// // Invalid

// test('input = 0.3', () => {
//     expect(fizzbuss(0.3)).toThrow("Please enter an integer");
// })

// test('input = 0', () => {
//     expect(fizzbuss(0)).toThrow("ConfigError");
// })

// test('input = abcde', () => {
//     expect(fizzbuss("abcde")).toThrow("Please enter an integer");
// })

// test('input = Infinity', () => {
//     expect(fizzbuss(Infinity)).toThrow("Please enter an integer");
// })

// test('input = -Infinity', () => {
//     expect(fizzbuss(-Infinity)).toThrow("Please enter an integer");
// })



// // Valid
// test('input = 1', () => {
//     expect(fizzbuss(1)).toBe("1")
// })

// test('input = 2', () => {
//     expect(fizzbuss(2)).toBe("2")
// })

// test('input = 3', () => {
//     expect(fizzbuss(3)).toBe("Fizz")
// })

// test('input = 4', () => {
//     expect(fizzbuss(4)).toBe("4")
// })

// test('input = 5', () => {
//     expect(fizzbuss(5)).toBe("Buzz")
// })

// test('input = 15 for fizz buzz game', () => {
//     expect(fizzbuss(15)).toBe("Fizz Buzz");
// })


// test("fizz buzz valid cases", (num: any) => {
//     let result = 1;

//     for (let i = 0; i < 16; i++) {
//         if (i % 3 === 0 && i % 5 === 0) {
//             return result += "Fizz Buzz";
//         } else if (i % 5 === 0) {
//             return result += "Buzz"
//         } else if (i % 3 === 0) {
//             return result += "Fizz"
//         } else {
//             return result++
//         }
//     }
// })

// // test("fizz buzz invalid cases", () => { })