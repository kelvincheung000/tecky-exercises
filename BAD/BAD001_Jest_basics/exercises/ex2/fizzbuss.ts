export function fizzBuzz(input: number) {
    if (input == 0) {
        throw new Error("Input should not be 0")
    }

    for (let i = 0; i < 15; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            return "Fizz Buzz"
        } else if (i % 5 === 0) {
            return "Buzz"
        } else if (i % 3 === 0) {
            return "Fizz"
        }
    }
}


export function checkFizzBuzz(currentNum: number, userInput: string, responseTime: number) {
    const maxResponseTime = 1500;
    if (responseTime > maxResponseTime) {
        return false;
    }

    let answer = fizzBuzz(currentNum);
    let isCorrect = answer == userInput ? true : false;
    if (!isCorrect) {
        return false;
    }

    return true;
}
