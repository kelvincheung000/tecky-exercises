import { factorial, fibonacci } from './factorial_fibonacci'

describe("testing on factorial", () => {
    // Positive
    it("can handle input 0", () => {
        expect(factorial(0)).toBe(1);
    });
    it("can handle input 1", () => {
        expect(factorial(1)).toBe(1);
    });
    it("can handle input 10", () => {
        expect(factorial(10)).toBe(3628800);
    });
    it("can handle input 5", () => {
        expect(factorial(5)).toBe(120);
    });

    // Negative
    it("can handle float", () => {
        expect(() => {
            factorial(3.4);
        }).toThrow("Should not be a float number");
    });
    it("can handle infinity", () => {
        expect(() => {
            factorial(Number.POSITIVE_INFINITY);
        }).toThrow("Should not handle infinity");
        expect(() => {
            factorial(Number.NEGATIVE_INFINITY);
        }).toThrow("Should not handle infinity");
    });
})


describe("testing on fibonacci", () => {
    // Negative
    it("can handle float", () => {
        expect(() => {
            fibonacci(5.6);
        }).toThrow("Should not handle float number");
    });
    it("can handle infinity", () => {
        expect(() => {
            fibonacci(Number.POSITIVE_INFINITY);
        }).toThrow();
        expect(() => {
            fibonacci(Number.POSITIVE_INFINITY);
        }).toThrow("Should not handle infinity");

        expect(() => {
            fibonacci(Number.NEGATIVE_INFINITY);
        }).toThrow("Should not handle infinity");
    });

    // Positive

    it("can return normal result", () => {
        expect(fibonacci(10)).toBe(55);
        expect(fibonacci(12)).toBe(144);
        expect(fibonacci(200)).toBeCloseTo(2.8057117299251E+41, -30);
        expect(fibonacci(2000)).toBeCloseTo(Number.POSITIVE_INFINITY, -30);
    });
    it("can handle boundary case ", () => {
        expect(fibonacci(1)).toBe(1);
        expect(fibonacci(2)).toBe(1);
    });
    it("can handle adnormal boundary case ", () => {
        expect(() => {
            fibonacci(201);
        }).toThrow();
        expect(() => {
            fibonacci(201);
        }).toThrow("Exceed limit");
    });
});



// // Q1
// test('(1 - 1) * 1 equal to 1', () => {
//     expect(factorial(1)).toBe(1);
// });

// test('(3 - 1) * 3 equal to 6', () => {
//     expect(factorial(3)).toBe(6);
//     expect(factorial(20)).toBeCloseTo(2.432902e+18, -11);
// });

// // Q2
// test('1 - 1) + (2 - 2) equal to 1', () => {
//     expect(fibonacci(1)).toBe(1);
// })

// test('num = 3; equal to 2', () => {
//     expect(fibonacci(3)).toBe(2);
// })

// // Fibonacci Calculator:
// // https://www.calculatorsoup.com/calculators/discretemathematics/fibonacci-calculator.php


// // Truthiness
// test('null', () => {
//     const n = null;
//     expect(n).toBeNull();
//     expect(n).toBeDefined();
//     expect(n).not.toBeUndefined();
//     expect(n).not.toBeTruthy();
//     expect(n).toBeFalsy();
// });

// test('zero', () => {
//     const z = 0;
//     expect(z).not.toBeNull();
//     expect(z).toBeDefined();
//     expect(z).not.toBeUndefined();
//     expect(z).not.toBeTruthy();
//     expect(z).toBeFalsy();
// });