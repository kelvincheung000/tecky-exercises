export function factorial(num: number): number {
    if (num < 0) {
        throw new Error("Should be positive integer");
    }

    if (num == Number.POSITIVE_INFINITY || num == Number.NEGATIVE_INFINITY) {
        throw new Error("Should not handle infinity");
    }
    if (!Number.isInteger(num)) {
        throw new Error("Should not handle float number");
    }

    if (num == 0 || num == 1) {
        return 1;
    }
    return factorial(num - 1) * num
}

export function fibonacci(num: number): number {
    const max = 200
    if (Math.round(num) !== num) {
        throw new Error("Should not handle float number");
    }

    if (num == Number.POSITIVE_INFINITY || num == Number.NEGATIVE_INFINITY) {
        throw new Error("Should not handle float number");
    }

    if (num >= max) {
        throw new Error("Exceed limit")
    }

    if (num == 1 || num == 2) {
        return 1;
    }
    return fibonacci(num - 1) + fibonacci(num - 2)
}

