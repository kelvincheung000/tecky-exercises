import express, { Request, Response } from "express";
import formidable from "formidable";
import { mkdirSync } from "fs";
import { MemoService } from "../services/MemoServices";
import { Server as SocketIO } from 'socket.io';
import { stringify } from "querystring";
// import { MemoRoutes } from '../routes/memo'

let uploadDir = "uploads";
mkdirSync(uploadDir, { recursive: true });

let form = formidable({
    uploadDir,
    allowEmptyFiles: true,
    maxFiles: 1,
    maxFileSize: 200 * 1024 ** 2,
    keepExtensions: true,
    filter: (part) => part.mimetype?.startsWith("image/") || false,
});


export class MemoController {

    private memoService: MemoService;
    private io: SocketIO;

    constructor(memoService: MemoService, io: SocketIO) {
        this.memoService = memoService;
        this.io = io;
    }


    deleteMemoById = async (req: express.Request, res: express.Response) => {
        try {
            let memoId = Number(req.params.id);
            if (!memoId) {
                res.status(401).json({
                    message: "Invalid ID",
                });
                return;
            }
            await this.memoService.deleteMemoById(memoId)
            res.json({
                message: "Add memo success",
            });
            this.io.emit("should-get-new-memo");
        } catch (error) {
            res.status(500).json({
                message: "System error",
            });
        }
    }

    postMemo = (req: Request, res: Response) => {
        try {
            form.parse(req, async (err, fields, files) => {
                if (err) {
                    res.status(400).json({ error: String(err) });
                    return;
                }
                let content = fields.content;
                if (typeof content !== "string" || !content) {
                    res.status(400).json({ error: "invalid content, expect an non-empty string" });
                    return;
                }

                let file = Array.isArray(files.image) ? files.image[0] : files.image;
                let image = file ? file.newFilename : undefined;

                await this.memoService.addMemo(content, image);

                res.json({
                    message: "Add memo success",
                });
                this.io.emit("should-get-new-memo");
            });
        } catch (error) {
            res.status(500).json({
                message: "Internal server error",
            });
        }
    }


    updateMemo = async (req: Request, res: Response) => {
        let { id, content } = req.body;
        let memoId = Number(id);

        if (!memoId) {
            res.json({
                message: "Invalid memo id",
            });
        }

        await this.memoService.getMemos(memoId, content)
        res.json({
            message: "updated success",
        });
    }

    getMemo = async (req: Request, res: Response) => {
        let memos = await this.memoService.getMemos();
        res.json(memos);
    }
}