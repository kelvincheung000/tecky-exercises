import express from "express";
import { checkPassword, hashPassword } from "../utils/hash";
import { UserService } from "../services/User"
// import { uuidv4 } from "uuidv4";


export class UserController {
    constructor(private userService: UserService) { }

    login = async (req: express.Request, res: express.Response) => {
        try {
            let { username, password } = req.body;
            if (!username) {
                res.status(400).json({ error: "missing username" });
                return;
            }
            if (!password) {
                res.status(400).json({ error: "missing password" });
                return;
            }

            let dbUser = await this.userService.getUserByUserName(username)

            console.log("dbUser = ", dbUser);
            console.log({ username, password });

            // let user = users.data.find(user => user.username === username)
            if (!dbUser) {
                res.status(404).json({ error: "user not found" });
                return;
            }
            let isMatched = await checkPassword(password, dbUser.password);

            if (!isMatched) {
                res.status(403).json({ error: "wrong password" });
                return;
            }
            req.session["username"] = username;
            res.json({
                message: "login successfully",
            });
        } catch (error) {
            res.status(500).json({
                message: "Internal system error",
            });
        }

        // res.redirect('/')
    }

    getMe = (req: express.Request, res: express.Response) => {
        let currentUser = req.session["username"] ? req.session["username"] : "NA";
        res.json({
            user: currentUser,
        });
    }

    logout = (req: express.Request, res: express.Response) => {
        let username = req.session["username"];
        console.log(`${username} want to logout`);
        req.session.destroy((error) => {
            if (error) {
                console.error("failed to destroy session:", error);
            }

            res.redirect(`/index.html?user=${username}`);
        });
    }

    googleLogin = async (req: express.Request, res: express.Response) => {
        try {
            const accessToken = req.session?.["grant"].response.access_token;

            const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            });
            const result = await fetchRes.json();
            console.log("google result = ", result);

            let user = await this.userService.getUserByEmail(result.email)
            if (!user) {
                // let randomString = uuidv4()
                let randomHashPassword = await hashPassword("1234")
                let email = result.email

                await this.userService.createUser(email, randomHashPassword)

            }
            if (req.session) {
                req.session["username"] = result.email;
            }
            res.redirect("/");
        } catch (error) {
            res.status(500).json({
                message: 'Google login fail'
            })
        }
    }

}
