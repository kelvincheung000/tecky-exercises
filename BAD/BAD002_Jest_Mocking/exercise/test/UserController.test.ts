import { UserController } from "../controllers/User"
import { UserService } from "../services/User"
import { Request, Response } from "express"
import pg from "pg";
import { checkPassword } from "../utils/hash";

jest.mock("../service/User")
jest.mock("../utils/hash")

describe("User Controller Tests", () => {
    let service: UserService;
    let controller: UserController;
    let req: Request;
    let res: Response;
    let status: any;
    let client: pg.Client;
    beforeEach(() => {
        client = {} as pg.Client
        service = new UserService(client)
        controller = new UserController(service)
        status = {
            json: jest.fn(() => null),
        }
        res = {
            status: jest.fn(() => { return status }),
            json: jest.fn(() => null),
            redirect: jest.fn(() => null)
        } as any as Response;
        let user = { id: 1 }
        jest.spyOn(service, "getUserByUserName").mockImplementation(async () => user)
    })

    test("Login Success", async () => {
        req = {
            body: {
                username: "kelvin",
                password: "kelvin"
            },
            session: {}
        } as unknown as Request
        (checkPassword as jest.Mock).mockReturnValue(true)
        await controller.login(req, res);
        expect(service.getUserByUserName).toBeCalledWith("kelvin");
        // expect(res.status).toBeCalledWith(500)
        expect(res.json).toBeCalledWith({ message: "login successfully" })
    })
    test("Login Failed - Password not Found", async () => {
        req = {
            body: {
                username: "kelvin",
            },
            session: {}
        } as unknown as Request
        await controller.login(req, res);
        expect(res.status).toBeCalledWith(400);
        expect(res.status(400).json).toBeCalledWith({ error: "missing password" });
    })
    test("Get Me", async () => {
        req = {
            session: { "username": "kelvin" }
        } as unknown as Request
        await controller.getMe(req, res);
        expect(res.json).toBeCalledWith({ user: "kelvin" })
    })

})