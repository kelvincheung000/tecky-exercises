export type User = {
    username: string;
    password: string;
    created_at: Date;
    updated_at: Date;
};
