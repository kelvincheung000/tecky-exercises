export type Memo = {
    id: number;
    content: string;
    image?: string;
    created_at: Date;
    updated_at: Date;

};