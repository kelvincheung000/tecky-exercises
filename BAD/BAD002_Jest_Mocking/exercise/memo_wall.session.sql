CREATE TABLE memos(
    id SERIAL primary key,
    content TEXT not null,
    image VARCHAR(255),
    created_at TIMESTAMP not null,
    updated_At TIMESTAMP not null
);
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) not null,
    password VARCHAR(255) not null,
    created_at TIMESTAMP not null,
    updated_At TIMESTAMP not null
);
select *
from memos;