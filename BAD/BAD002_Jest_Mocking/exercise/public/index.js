let title = document.querySelector('.title')
let words = title.textContent.split('')
const socket = io.connect();
title.textContent = ''

for (let word of words) {
  let div = document.createElement('div')
  div.className = 'word'
  div.textContent = word
  title.appendChild(div)
}

let memoListElem = document.querySelector('.memo-list')
let memoTemplate = memoListElem.querySelector('.memo')



document.querySelector('#memo-form')
  .addEventListener('submit', async (event) => {
    event.preventDefault(); // To prevent the form from submitting synchronously
    const form = event.target;
    let formData = new FormData(form)
    //... create your form object with the form inputs
    const res = await fetch('/memos', {
      method: "POST",
      body: formData
    });
    console.log(res.status)
    let data = await res.json()
    console.log(data);
    if (res.status === 200) {
      form.reset()
      // window.location.reload()
      // fetchMemo()
    }

  });

document.querySelector('#login-form')
  .addEventListener('submit', async (event) => {
    event.preventDefault(); // To prevent the form from submitting synchronously

    // preparation
    const form = event.target;
    let formObj = {
      username: form.username.value,
      password: form.password.value

    }

    // send request
    const res = await fetch('/login', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(formObj)
    });

    // chcek result
    console.log(res.status)
    let data = await res.json()
    console.log(data);
    if (res.status === 200) {
      form.reset()
      // window.location.reload()
      // fetchMemo()
      console.log('login success');
      window.location = '/admin/admin.html?name=Ben'
    }

  });


async function fetchMemo() {

  let res = await fetch('/memos')
  let memos = await res.json()


  let meRes = await fetch('/me')
  let meData = await meRes.json()
  let { user } = meData



  // memoTemplate.remove()
  document.querySelector('.memo-list').innerHTML = ''
  for (let memo of memos) {
    // let memoContainer = memoTemplate.cloneNode(true)
    let memoHTML = `  
      <div id='memo_${memo.id}' class="memo">
        <div class='memo-inner'>
        ${user === 'NA'
        ? `<div class="memo-content" >${memo.content}</div>`
        : `<div class="memo-content" contentEditable>${memo.content}</div>`}
          ${getImageTag(memo.image)}
        </div>
        <button class='delete-btn' onclick="deleteMemo(${memo.id})">
          <i class="bi bi-trash3-fill"></i>
        </button>
        <button class='edit-btn'  onclick="editMemo(${memo.id})">
          <i class="bi bi-pencil-square"></i>
        </button>
      </div >
    `

    memoListElem.innerHTML += memoHTML
    // memoContainer.querySelector('.memo-content').textContent = memo.content
    // let img = memoContainer.querySelector('img')
    // if (memo.image) {
    //   img.src = '/uploads/' + memo.image
    // } else {
    //   img.remove()
    // }
    // memoListElem.appendChild(memoContainer)
  }
}

async function editMemo(memoId){
  let newContent = document.querySelector(`#memo_${memoId}`).querySelector('.memo-inner').innerText
  console.log("I want to edit memoId:", memoId);
  console.log({newContent});


  let updateRes = await fetch(`/memos`,{
    method: 'PATCH',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({
      content: newContent,
      id: memoId
    })
  })
  if (updateRes.ok){
    fetchMemo()
  }

}

async function deleteMemo(memoId){

  let updateRes = await fetch(`/memos/${memoId}`,{
    method: 'DELETE'
  })
}

function getImageTag(image) {
  if (!image) {
    return ""
  } else {
    return `<img alt='user submitted image' src='/uploads/${image}'>`
  }
}

fetchMemo()



socket.on('should-get-new-memo', (newMemo)=>{
  fetchMemo()
})


window.onload  = function(){

  const searchParams = new URLSearchParams(window.location.search);
    const user = searchParams.get('user');

    if (user){
        alert(user + ' 你成功logout')
    }
}