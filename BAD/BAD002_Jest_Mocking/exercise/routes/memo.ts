import express, { Request, Response } from "express";
import formidable from "formidable";
import { mkdirSync } from "fs";
import { client } from "../utils/db";
import { io } from "../server";
import { Memo } from "../models/memo";

export let memoRoutes = express.Router();

let uploadDir = "uploads";

mkdirSync(uploadDir, { recursive: true });

let form = formidable({
    uploadDir,
    allowEmptyFiles: true,
    maxFiles: 1,
    maxFileSize: 200 * 1024 ** 2,
    keepExtensions: true,
    filter: (part) => part.mimetype?.startsWith("image/") || false,
});



let deleteMemoById = async (req: express.Request, res: express.Response) => {
    try {
        let memoId = Number(req.params.id);
        if (!memoId) {
            res.status(401).json({
                message: "Invalid ID",
            });
            return;
        }
        await client.query("delete from memos where id = $1", [memoId]);
        res.json({
            message: "Add memo success",
        });
        io.emit("should-get-new-memo");
    } catch (error) {
        res.status(500).json({
            message: "Sysem error",
        });
    }
}

let postMemo = (req: express.Request, res: express.Response) => {
    try {
        form.parse(req, async (err, fields, files) => {
            if (err) {
                res.status(400).json({ error: String(err) });
                return;
            }
            let content = fields.content;
            if (typeof content !== "string" || !content) {
                res.status(400).json({ error: "invalid content, expect an non-empty string" });
                return;
            }

            let file = Array.isArray(files.image) ? files.image[0] : files.image;
            let image = file ? file.newFilename : undefined;

            await client.query(
                "INSERT INTO memos (content,image, created_at, updated_at) values ($1,$2, current_timestamp, current_timestamp)",
                [content, image]
            );

            res.json({
                message: "Add memo success",
            });
            io.emit("should-get-new-memo");
            // res.redirect('/')
        });
    } catch (error) {
        res.status(500).json({
            message: "Internal server error",
        });
    }
}


let updateMemo = async (req: Request, res: Response) => {
    let { id, content } = req.body;
    let memoId = Number(id);

    if (!memoId) {
        res.json({
            message: "Invalid memo id",
        });
    }

    await client.query(
        /*sql */ `update memos set content = $1, updated_at = current_timestamp where id = $2`,
        [content, memoId]
    );
    res.json({
        message: "updated success",
    });
}

let getMemo = async (req: Request, res: Response) => {
    let memoReusltFromDB = await client.query(/*sql*/ " select * from memos order by updated_at desc;");
    let memos: Memo[] = memoReusltFromDB.rows
    res.json(memos);
}
memoRoutes.get("/memos", getMemo);
memoRoutes.patch("/memos", updateMemo);
memoRoutes.post("/memos", postMemo);
memoRoutes.use("/uploads", express.static(uploadDir));
memoRoutes.delete("/memos/:id", deleteMemoById);

