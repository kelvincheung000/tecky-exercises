import express from "express";
// import { red, reset } from "asciichart";
import { client } from "../utils/db";
import { UserService } from "../services/User";
import { UserController } from "../controllers/User"

export let userRoutes = express.Router();

userRoutes.use(express.urlencoded({ extended: false }));


// let googleLogin = async (req: express.Request, res: express.Response) => {
//     try {
//         const accessToken = req.session?.["grant"].response.access_token;

//         const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
//             headers: {
//                 Authorization: `Bearer ${accessToken}`,
//             },
//         });
//         const result = await fetchRes.json();
//         console.log("google result = ", result);

//         const users = (await client.query(`SELECT * FROM users WHERE users.username = $1`, [result.email])).rows;
//         const user: User = users[0];
//         if (!user) {
//             let randomString = uuidv4()
//             let randomHashPassword = await hashPassword(randomString)
//             await client.query(`
//             INSERT INTO users (username, password, create_at, ) 
//             values ($1, $2, current_timestamp, current_timestamp)`,
//                 [result.email, randomHashPassword],

//             );
//         }
//         if (req.session) {
//             req.session["username"] = result.email;
//         }
//         res.redirect("/");
//     } catch (error) {
//         res.status(500).json({
//             message: 'Google login fail'
//         })
//     }
// }

// let login = async (req: express.Request, res: express.Response) => {
//     try {
//         let { username, password } = req.body;
//         if (!username) {
//             res.status(400).json({ error: "missing username" });
//             return;
//         }
//         if (!password) {
//             res.status(400).json({ error: "missing password" });
//             return;
//         }

//         let userResult = await client.query(/*sql */ `select * from users where username = $1`, [username]);
//         let dbUser = userResult.rows[0];

//         console.log("dbUser = ", dbUser);
//         console.log({ username, password });

//         // let user = users.data.find(user => user.username === username)
//         if (!dbUser) {
//             res.status(404).json({ error: "user not found" });
//             return;
//         }
//         let isMatched = await checkPassword(password, dbUser.password);

//         if (!isMatched) {
//             res.status(403).json({ error: "wrong password" });
//             return;
//         }
//         req.session["username"] = username;
//         res.json({
//             message: "login successfully",
//         });
//     } catch (error) {
//         res.status(500).json({
//             message: "Internal system error",
//         });
//     }

//     // res.redirect('/')
// }

// let getMe = (req: express.Request, res: express.Response) => {
//     let currentUser = req.session["username"] ? req.session["username"] : "NA";
//     res.json({
//         user: currentUser,
//     });
// }
// let logout = (req: express.Request, res: express.Response) => {
//     let username = req.session["username"];
//     console.log(`${username} want to logout`);
//     req.session.destroy((error) => {
//         if (error) {
//             console.error("failed to destroy session:", error);
//         }

//         res.redirect(`/index.html?user=${username}`);
//     });
// }

let userService = new UserService(client)
let userController = new UserController(userService);

userRoutes.post("/login", userController.login);
userRoutes.get("/me", userController.getMe);
userRoutes.post("/logout", userController.logout);
userRoutes.get("/login/google", userController.googleLogin);