import pg from "pg";

export class UserService {
    private client;

    constructor(client: pg.Client) {
        this.client = client;
    }

    async getUserByUserName(username: string) {
        let userResult = await this.client.query(/*sql */ `select * from users where username = $1`, [username]);
        let dbUser = userResult.rows[0];
        return dbUser;
    }

    async getUserByEmail(email: string) {
        const users = (await this.client.query(`SELECT * FROM users WHERE users.username = $1`, [email])).rows;
        const user = users[0];
        return user;
    }

    async createUser(email: string, randomHashPassword: string) {
        await this.client.query(`
                INSERT INTO users (username, password, created_at, updated_at) 
                values ($1, $2, current_timestamp, current_timestamp)`,
            [email, randomHashPassword],
        );
    }

}