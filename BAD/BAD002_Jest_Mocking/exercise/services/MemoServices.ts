import pg from 'pg';
import { Memo } from "../models/memo"

export class MemoService {
    constructor(private client: pg.Client) { }
    public async deleteMemoById(memoId: number) {
        await this.client.query("delete from memos where id = $1", [memoId]);
    }
    public async addMemo(content: string, image: string | undefined) {
        await this.client.query(
            "INSERT INTO memos (content,image, created_at, updated_at) values ($1,$2, current_timestamp, current_timestamp)",
            [content, image]
        )
    }
    public async updateMemo(memoId: number, content: string) {
        await this.client.query(
            /*sql */ `update memos set content = $1, updated_at = current_timestamp where id = $2`,
            [content, memoId]
        )
    }
    public async getMemos() {
        let memoReusltFromDB: Memo[] = (await this.client.query(/*sql*/ " select * from memos order by updated_at desc;")).rows;
        return memoReusltFromDB;
    }
}


