import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    // 有次序之分; 先整左個無foreign key 既table，然後再整有foreign key 既table，因為後者依賴前者

    if (!(await knex.schema.hasTable("users"))) {
        await knex.schema.createTable("users", (table) => {
            table.increments();
            table.string("username");
            table.string("password");
            table.string("level");
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("categories"))) {
        await knex.schema.createTable("categories", (table) => {
            table.increments();
            table.string("name");
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("files"))) {
        await knex.schema.createTable("files", (table) => {
            table.increments();
            table.string("name");
            table.text("Content");
            table.integer("is_file");
            // FOREIGN KEY is created automatically if we are using the function references
            table.integer("category_id").unsigned().notNullable();
            table.integer("owner_id").unsigned().notNullable();
            table.foreign("category_id").references("categories.id");
            table.foreign("owner_id").references("users.id");
            table.timestamps(false, true);
        })
    }

}

export async function down(knex: Knex): Promise<void> {
    // 有次序之分;
    // down function would delete the files table -> categories table -> users table from the up function
    await knex.schema.dropTableIfExists("files");
    await knex.schema.dropTableIfExists("categories");
    await knex.schema.dropTableIfExists("users");
}

