import { Knex } from "knex";
import XLSX from "XLSX"
import { hashPassword } from "../hash";

interface User {
    username: string;
    password: string;
    level: string;
}

interface Category {
    name: string;
}

interface File {
    name: string;
    Content: string;
    is_file: number;
    category: string;
    owner: string;
}

class FileDB {
    public static OwnersMap = new Map(); // ??
    public static CategoriesMap = new Map(); // ??
    public name: string;
    public Content?: string;
    public is_file: number;
    public category_id: number;
    public owner_id: number;
    constructor(file: File) {
        this.name = file.name;
        this.Content = file.Content;
        this.is_file = file.is_file;
        this.category_id = FileDB.CategoriesMap[file.category] ? FileDB.CategoriesMap[file.category] : -1;
        if (FileDB.OwnersMap[file.owner])
            this.owner_id = FileDB.OwnersMap[file.owner];
        else
            this.owner_id = -1;
    }
    public static async InitializeMaps(txn: Knex.Transaction, categories: Category[], users: User[]) {
        for (let user of users) {
            let results = await txn("users").where({ username: user.username });
            if (results) {
                FileDB.OwnersMap[user.username] = results[0].id;
            } else {
                throw new Error("User not found");
            }
        }
        for (let category of categories) {
            let results = await txn("categories").where({ name: category.name });
            if (results) {
                FileDB.CategoriesMap[category.name] = results[0].id;
            } else {
                throw new Error("Categories not found");
            }
        }
    }
}

export async function seed(knex: Knex): Promise<void> {
    const txn = await knex.transaction();
    try {
        let workbook = XLSX.readFile("./seeds/BAD004-exercise.xlsx");
        let usersWs = workbook.Sheets["user"];
        let filesWs = workbook.Sheets["file"];
        let categoriesWs = workbook.Sheets["category"];

        let users: User[] = XLSX.utils.sheet_to_json(usersWs);
        let categories: Category[] = XLSX.utils.sheet_to_json(categoriesWs);
        let files: File[] = XLSX.utils.sheet_to_json(filesWs);

        await txn("files").del();
        await txn("categories").del();
        await txn("users").del();

        // User
        const usersHashed: User[] = [];
        for (let user of users) {
            let hashedPassword = await hashPassword(user.password.toString())
            user.password = hashedPassword
            if (user.username !== "")
                usersHashed.push(user);
        }
        await txn("users").insert(usersHashed).returning("id");

        // Category
        await txn("categories").insert(categories).returning("id")

        // File
        await FileDB.InitializeMaps(txn, categories, users)
        const filesDB: FileDB[] = [];
        for (let file of files) {
            let fileDB = new FileDB(file);
            if (fileDB.owner_id > 0 && fileDB.category_id > 0) {
                filesDB.push(fileDB);
            }
            await txn("files").insert(filesDB).returning("id");
            await txn.commit();
        }
    } catch (error) {
        console.log(error);
        await txn.rollback();
    };
    await knex.destroy();
}



