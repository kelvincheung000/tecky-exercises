import Knex from "knex";
const knexConfigs = require('./knexfile');
const environment = process.env.NODE_ENV || 'development';
const knexConfig = knexConfigs[environment];
export const knex = Knex(knexConfig);

