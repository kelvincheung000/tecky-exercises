export type Memo = {
    id: number;
    content: string;
    image?: string;
    create_at: Date;
    update_at: Date;

};