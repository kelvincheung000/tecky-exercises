import { Knex } from "knex";



export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable("memos"))) {
        await knex.schema.createTable("memos", (table) => {
            table.increments();
            table.string("name");
            table.text("content");
            table.text("image").nullable();
            table.timestamps(false, true);
        });
    }
    if (!(await knex.schema.hasTable("users"))) {
        await knex.schema.createTable("users", (table) => {
            table.increments();
            table.string("username");
            table.string("password");
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("users");
    await knex.schema.dropTableIfExists("memos");
}

