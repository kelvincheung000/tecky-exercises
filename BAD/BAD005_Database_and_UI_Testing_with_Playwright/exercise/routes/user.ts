import express from "express";
import {UserController} from '../controllers/User'
import {UserService} from '../services/User'
import {Knex} from "knex";

export let userRoutes = express.Router();

userRoutes.use(express.urlencoded({ extended: false }));

export function InitializeUserRoutes(client:Knex){
    let userService = new UserService(client)
    let userController = new UserController(userService)
    userRoutes.post("/login",userController.login);
    userRoutes.get("/me", userController.getMe);
    userRoutes.post("/logout", userController.logout);
    userRoutes.get("/login/google", userController.googleLogin);
}