import express from "express";
import {Server as SocketIO} from 'socket.io';
import { MemoService } from "../services/Memo";
import { MemoController } from "../controllers/Memo";
import {Knex} from "knex";

export let memoRoutes = express.Router();
export class MemoRoutes{
    static readonly UPLOAD_DIR="uploads";
    public static InitializeMemoRoutes(client:Knex,io:SocketIO){
        let service = new MemoService(client)
        let controller = new MemoController(service,io)
        memoRoutes.get("/memos", controller.getMemos);
        memoRoutes.patch("/memos",controller.updateMemo );
        memoRoutes.post("/memos", controller.postMemo);
        memoRoutes.use("/uploads", express.static(MemoRoutes.UPLOAD_DIR));
        memoRoutes.delete("/memos/:id", controller.deleteMemoById);
    }
}

