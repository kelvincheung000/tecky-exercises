import express from "express";
import { checkPassword } from "../utils/hash";
import {UserService} from "../services/User"
import { User } from "../models/user";
import fetch from 'node-fetch'

export class UserController {
    constructor(private userService: UserService){}

    login = async (req: express.Request, res: express.Response) => {
        try {
            let { username, password } = req.body;
            if (!username) {
                res.status(400).json({ error: "missing username" });
                return;
            }
            if (!password) {
                res.status(400).json({ error: "missing password" });
                return;
            }

            let dbUser= await this.userService.getUserByUsername(username)

            console.log("dbUser = ", dbUser);
            console.log({ username, password });

            // let user = users.data.find(user => user.username === username)
            if (!dbUser) {
                res.status(404).json({ error: "user not found" });
                return;
            }
            let isMatched = await checkPassword(password, dbUser.password);
            console.log(isMatched);
            if (!isMatched) {
                res.status(403).json({ error: "wrong password" });
                return;
            }
            req.session["username"] = username;
            res.json({
                message: "login successfully",
            });
        } catch (error) {
            res.status(500).json({
                message: "Internal system error",
            });
        }
    }

    googleLogin = async (req : express.Request, res : express.Response) => {
        try {
            const accessToken = req.session?.["grant"].response.access_token;

            const googleLoginInfo=await this.userService.getGoogleLoginInfo(accessToken)
    
            const users=await this.userService.getUserByUsername(googleLoginInfo.email)
            const user : User = users[0];
            
            if(!user){
                await this.userService.addUser(googleLoginInfo.email)
            }
            if(req.session){
                req.session["username"] = googleLoginInfo.email;
            }
            res.redirect("/");
        } catch (error) {
            res.status(500).json({
                message:'Google login fail'
            })
        }
    }
    getMe = (req : express.Request, res : express.Response) => {
        let currentUser = req.session["username"] ? req.session["username"] : "NA";
        res.json({
            user: currentUser,
        });
    }
    logout = (req : express.Request, res : express.Response) => {
        let username = req.session["username"];
        console.log(`${username} want to logout`);
        req.session.destroy((error) => {
            if (error) {
                console.error("failed to destroy session:", error);
            }
            
            res.redirect(`/index.html?user=${username}`);
        });
    }
}
