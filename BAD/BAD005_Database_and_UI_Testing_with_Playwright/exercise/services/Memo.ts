import { Knex } from "knex";
import { Memo } from "../models/memo";

export class MemoService {
    constructor(private knex: Knex) { }

    public async deleteMemoById(memoId: number) {
        await this.knex("memos").where({ id: memoId }).del()
    }
    public async addMemo(content: string, image: string | undefined) {
        await this.knex("memos").insert({ content, image });
    }
    public async updateMemo(memoId: number, content: string) {
        await this.knex("memos").update({ content }).where({ id: memoId });
    }
    public async getMemos() {
        let memoReusltFromDB: Memo[] = await this.knex("memos").orderBy("updated_at", "desc");
        return memoReusltFromDB;
    }
}