
import {Knex} from "knex";
import { hashPassword } from "../utils/hash";

export class UserService {
    private knex;
    
    constructor(client: Knex) {
        this.knex = client;
    }
    
    async getUserByUsername(username: string){
        let userResult=await this.knex("users").where({username});
        let dbUser = userResult[0];
        return dbUser;
    }
    async addUser(username: string,password:string|undefined=undefined){
        let randomHashPassword = null;
        if(password === undefined)
            randomHashPassword =null;
        else
            randomHashPassword=await hashPassword(password)
        await this.knex("users").insert({username:username,password:randomHashPassword});
    }

    async getGoogleLoginInfo(accessToken: string){
        const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        });
        const result = await fetchRes.json();
        console.log("google result = ", result);
        return result
    }
}