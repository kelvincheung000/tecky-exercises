import KnexInit from "knex";
import { Knex } from "knex";
import dotenv from "dotenv";
import XLSX from "XLSX";
import { hashPassword } from "../utils/hash";


dotenv.config();

const knexConfigs = require("../knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = KnexInit(knexConfig);
interface User {
    username: string;
    password: string;
}
interface Memo {
    content: string;
    image?: string;
}
export async function seed(knex: Knex): Promise<void> {
    let workbook = XLSX.readFile("./seeds/data.xlsx");
    let userWs = workbook.Sheets["user"];
    let memoWs = workbook.Sheets["memo"];
    // Deletes ALL existing entries
    await knex("memos").del();
    await knex("users").del();

    let users: User[] = XLSX.utils.sheet_to_json(userWs);
    let memos: Memo[] = XLSX.utils.sheet_to_json(memoWs);

    const usersHashed: User[] = [];
    for (let user of users) {
        let hashedPassword = await hashPassword(user.password)
        user.password = hashedPassword
        usersHashed.push(user);
    }
    await knex("users").insert(usersHashed);
    await knex("memos").insert(memos);
};
