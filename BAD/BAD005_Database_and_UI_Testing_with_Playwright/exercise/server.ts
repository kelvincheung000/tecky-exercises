import express from 'express'
import expressSession from 'express-session'
import { print } from 'listening-on'
import { join, resolve } from 'path'
import {connectDB} from './utils/db'
import http from 'http';
import {Server as SocketIO} from 'socket.io';
import grant from 'grant';
import env from './utils/env'
import { counter, monitorRequest } from './utils/monitor'
import { adminGuard } from './utils/guard'
import { logger } from './utils/logger'
// import { client } from "./utils/db";
import { MemoRoutes, memoRoutes } from './routes/memo'
import { userRoutes,InitializeUserRoutes } from './routes/user'
import Knex from "knex";
import dotenv from "dotenv";

dotenv.config();

const knexConfigs = require("./knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = Knex(knexConfig);

const grantExpress = grant.express({
    "defaults":{
        "origin": "http://localhost:8100",
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": env.GOOGLE_CLIENT_ID,
        "secret": env.GOOGLE_CLIENT_SECRET,
        "scope": ["profile","email"],
        "callback": "/login/google"
    }
});


let app = express()
const server = new http.Server(app);
export const io = new SocketIO(server);

app.use(express.json())


app.use(
  expressSession({
    secret: env.SESSION_SECRET ,
    resave: true,
    saveUninitialized: true,
  }),
)

app.use(grantExpress as express.RequestHandler);

declare module 'express-session' {
  interface SessionData {
    counter?: number
    username?: string
  }
}

app.use(counter)
app.use(monitorRequest)

connectDB()
app.use(memoRoutes);
app.use(userRoutes);
InitializeUserRoutes(knex);
MemoRoutes.InitializeMemoRoutes(knex,io);



io.on('connection', function (socket) {
  // console.log(socket.id , " is connected to io server");
  logger.error(socket.id + " is connected to io server")
  logger.info(socket.id + " is connected to io server")
  logger.warn(socket.id + " is connected to io server")
  logger.debug(socket.id + " is connected to io server")
});

app.use(express.static('public'))
app.use('/admin', adminGuard, express.static('admin'))



app.use((req, res) => {
  res.sendFile(resolve(join('public', '404.html')))
})

let port = 8100
server.listen(port, () => {
  print(port)
})
