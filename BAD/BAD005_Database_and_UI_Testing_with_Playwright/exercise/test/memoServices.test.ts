import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["test"]);
import { MemoService } from '../services/Memo'
import { seed } from '../seeds/create-memo-wall-seed'

describe("MemoService", () => {
    let memoService: MemoService;


    it("should get init data", async () => {
        const memos = await memoService.getMemos();
        const memo = memos[0]

        console.log("memo = ", memo);
        expect(memos.length).toBe(1);
        expect(memo).toMatchObject({
            content: "Memo 1",
        });
    })
})

it("should get newly insert data", async () => {
    await knex
        .insert([
            {
                content: "Memo 2",
            },
            {
                content: "Memo 3",
            }
        ]).into("memos");
})


// describe("MemoService", () => {
//     let memoService: MemoService;

//     beforeEach(async () => {
//         await seed(knex)
//         memoService = new MemoService(knex);
//     })

//     // it("should delete a memo", async () => {
//     //     const memo = await memoService.deleteMemoById(memoId: 1)
//     //     expect(memo.id).toBe
//     // })

//     it("should get all memos", async () => {
//         const memo = await memoService.getMemos();
//         expect(memo.length).toBe(99);
//     })

//     it("should get newly insert data", async () => {
//         await knex
//             .insert([
//                 {
//                     content: "Memo 2",
//                 },
//                 {
//                     content: "Memo 3",
//                 },
//             ])
//             .into("memos");
//         const memos = await memoService.getMemos();
//         expect(memos.length).toBe(99)
//     })



// })
