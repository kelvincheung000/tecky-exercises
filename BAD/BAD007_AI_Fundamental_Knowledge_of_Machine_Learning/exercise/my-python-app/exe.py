from operator import index
import numpy as np
from scipy.stats import mode
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report


class KNNClassifier:

    def __init__(self, X_train, y_train, n_neighbors=3):
        self.n_neighbors = n_neighbors
        self._X_train = X_train
        self._y_train = y_train

    """calculate the euclidean distance here"""

    def euclidean_dist(self, point_1, point_2):
        index = 0
        sum = 0
        for p in point_1:
            p2 = point_2[index]
            sum += (p1-p2)*(p1-p2)
            index += 1
            pass
        return np.sqrt(sum)

    """accept multiple inputs here and predict one by one with predict_single()"""

    def predict(self, X_test_array):
        results = []
        for X_test in X_test_array:
            results.append(self.predict_single(X_test))
        return results

    """predict single input here"""

    def predict_single(self, input_data_single):
        smallest_distance = [9999999, 9999999, 9999999]
        labels = [-1, -1, -1]
        index = 0
        for x in self._X_train:
            distance = self.euclidean(input_data_single, x)
            for i in range(3):
                if distance < smallest_distance[i]:
                    smallest_distance[i] = distance
                    labels[i] = self._y_train[index]
                pass
            index += 1
        return mode(labels)[0][0]


iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42)

print(X_train.shape)    # (120, 4)
print(X_test.shape)     # (30, 4)
print(y_train.shape)    # (120,)
print(y_test.shape)     # (30,)

iris_knn_classifier = KNNClassifier(X_train, y_train)
y_pred = iris_knn_classifier.predict(X_test)
print(y_pred)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))
