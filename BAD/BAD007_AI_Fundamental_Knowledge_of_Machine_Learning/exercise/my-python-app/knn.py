import math
from operator import index
from turtle import distance
import numpy as np
from scipy.stats import mode
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report


class KNNClassifier:

    def __init__(self, X_train, y_train, n_neighbors=3):
        self.n_neighbors = n_neighbors
        self._X_train = X_train
        self._y_train = y_train

    """calculate the euclidean distance here"""

    def euclidean_dist(self, point_1, point_2):
        index = 0
        sigma = 0
        for p1 in point_1:
            p2 = point_2[index]
            sigma += (p1-p2)**2
            index += 1
            pass
        return math.sqrt(sigma)

    """accept multiple inputs here and predict one by one with predict_single()"""

    def predict(self, X_test_array):
        results = []
        for x_test in X_test_array:
            results.append(self.predict_single(x_test))
        return results

    """predict single input here"""

    def predict_single(self, input_data_single):
        knd_array = []
        index = 0
        for x in self._X_train:
            d = self.euclidean_dist(input_data_single, x)
            if len(knd_array) < self.n_neighbors:
                knd_array.append(
                    {"distance": d, "label": self._y_train[index]})
            else:
                knd_array_index = 0
                for knd in knd_array:
                    if d < knd["distance"]:
                        knd_array[knd_array_index] = {
                            "distance": d, "label": self._y_train[index]}
                        break
                    knd_array_index += 1
                    pass
            index += 1
            pass
        print(knd_array)
        pass
        most_class_count = [0, 0, 0]
        for flower in knd_array:
            most_class_count[flower["label"]] += 1
            pass
        return np.argmax(most_class_count)


iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42)


# (training sample, testing sample) <- must be different sets of data

print(X_train.shape)    # (120, 4)
print(X_test.shape)     # (30, 4)
print(y_train.shape)    # (120,)
print(y_test.shape)     # (30,)

iris_knn_classifier = KNNClassifier(X_train, y_train)
y_pred = iris_knn_classifier.predict(X_test)
# print(y_pred)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))
