import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('students').del();
    await knex('teachers').del();

    // Inserts seed entries
    const [{ id }]: Array<{ id: number }> = await knex.insert({
        teacher_name: "Bob",
        date_of_birth: "1970-01-01"
    }).into('teachers').returning('id');

    return await knex.insert([{
        student_name: "Peter",
        level: 25,
        date_of_birth: "1995-05-15",
        teacher_id: id
    }, {
        student_name: "John",
        level: 25,
        date_of_birth: "1985-06-16",
        teacher_id: id
    }, {
        student_name: "Simon",
        level: 25,
        date_of_birth: "1987-07-17",
        teacher_id: null
    }
    ]).into('students');
};
