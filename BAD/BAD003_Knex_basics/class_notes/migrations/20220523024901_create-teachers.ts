import { Knex } from "knex";

// up function is for the forward direction of the migration
export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("teachers");
    if (!hasTable) {
        await knex.schema.createTable("teachers", (table) => {
            table.increments();
            table.string("name");
            table.date("date_of_birth");
            table.timestamps(false, true);
        });
    }
}

// down function is for the backward direction of the migration
export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("teachers");
}

