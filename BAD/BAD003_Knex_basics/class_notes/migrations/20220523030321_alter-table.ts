import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("teachers")) {
        await knex.schema.alterTable("teachers", (table) => {
            table.renameColumn("name", "teacher_name");
            table.decimal("level", 3);
        });
    }
    if (await knex.schema.hasTable("students")) {
        await knex.schema.alterTable("students", (table) => {
            table.renameColumn("name", "student_name");
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("teachers")) {
        await knex.schema.alterTable("teachers", (table) => {
            table.renameColumn("teacher_name", "name");
            table.string("level");
        });
    }
    if (await knex.schema.hasTable("students")) {
        await knex.schema.alterTable("students", (table) => {
            table.renameColumn("student_name", "name");
        });
    }
}

