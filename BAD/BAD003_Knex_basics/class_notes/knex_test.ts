import Knex from "knex";
import dotenv from "dotenv";
// import { StudentService } from "./services/StudentService";

dotenv.config();

// class MySqlLib {
//     select() {
//         return this;
//     }
//     where() {
//         return this;
//     }
// }
// let mySql = new MySqlLib();
// mySql.select().where().where()


const knexConfigs = require("./knexfile");
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode];
const knex = Knex(knexConfig);

async function test() {
    const conditions = [{ student_name: "Peter", level: 25 }, { student_name: "John", level: 25 }]
    const result = await knex
        .select("student_name", "level", "date_of_birth")
        .from("students").where(conditions[0]);
    console.table(result);

    let query = knex
        .select("student_name", "level", "date_of_birth")
        .from("students")
    for (const condition of conditions) {
        query = query.orWhere(condition);
    }
    let results = await query;
    console.table(results);
}

async function test2() {

}
test()
