import numpy as np
print("Hello, world!")


def hi():
    a = 1
    if a == 1:
        print("Hello")
    a += 1
    return


gen = ((x, y, z) for x in range(1, 20) for y in range(1, 20) for z in range(1, 20) if x**2 + y**2=z**2)
# gen = np.array(gen)
# print(gen)

gen_human_readable = []
for x in range(1, 20):
    for y in range(1, 20):
        for z in range(1, 20):
            if x**2 + y**2 == z**2:
                gen_human_readable.append((x, y, z))
for result in gen:
    print(result)
for result in gen_human_readable:
    print(result)
