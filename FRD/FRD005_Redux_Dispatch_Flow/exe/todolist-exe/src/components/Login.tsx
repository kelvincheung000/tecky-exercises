import React from "react";
import { useState } from "react";

function Login() {
  const [username, setUsername] = useState("kelvin");
  const [password, setPassword] = useState("1234");

  // const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false)

  const onSubmit = (e: any) => {
    e.preventDefault();
    // let username = e.target.username.value
    // server checking
    if (username === "kelvin" && password === "1234") {
      // login successful
      // call redux ('login/checking', true)
      console.log("login successful");
    } else {
      // login failed
      console.log("login failed");
    }
  };

  return (
    <div>
      <form onSubmit={onSubmit}>
        <label>
          username:
          <input
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          ></input>
        </label>
        <br />
        <label>
          password:
          <input
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></input>
        </label>
        <button>Submit</button>
      </form>
    </div>
  );
}

export default Login;
