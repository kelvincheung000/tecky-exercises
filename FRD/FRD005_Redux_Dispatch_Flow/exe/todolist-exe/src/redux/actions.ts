import { createAction } from '@reduxjs/toolkit'

const increment = createAction<string>('todoItems/increment')
const updateCount = createAction<{ count: number, index: number }>('todoItems/count/update')
const login = createAction<boolean>('login/checking')

export { increment, updateCount, login } 