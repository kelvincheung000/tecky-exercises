import { createReducer } from "@reduxjs/toolkit";
import { ITodoState } from "./state";
import { increment, updateCount, login } from "./actions"

const initialState: ITodoState = {
    items: [
        {
            name: "Buy milk",
            count: 0
        },
        {
            name: "Buy banana",
            count: 2
        },
        {
            name: "Buy ABC",
            count: 3
        },
        {
            name: "Buy DEF",
            count: 4
        },
    ],
    waterL: 0,
    isLoggedIn: true
}

const todoItemReducer = createReducer(initialState, (builder) => {
    builder
        .addCase(updateCount, (state, action) => {
            const payload: {
                count: number,
                index: number
            } = action.payload
            state.items[payload.index].count = state.items[payload.index].count + payload.count
        })

        .addCase(increment, (state, action) => {
            // console.log("action: ", action.payload)
            state.waterL = 1000
            // [...state.items, action.payload]
        })

        .addCase(login, (state, action) => {
            // action.payload => true / false
            state.isLoggedIn = action.payload
        })

})

export default todoItemReducer