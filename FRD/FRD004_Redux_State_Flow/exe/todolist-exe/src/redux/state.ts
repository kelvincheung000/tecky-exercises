export interface TodoItem {
    name: string // => ["Buy milk", "Buy banana"]
    count: number
}

export interface ITodoState {
    items: TodoItem[]
    waterL: number
    isLoggedIn: boolean
}