// import { useSelector } from "react-redux"
import { ITodoState } from "../redux/state";
import { useSelector } from "react-redux";
import { updateCount } from "../redux/actions";
// interface Props {
//     items: string[]
// }

export default function TodoItem() {
  const items = useSelector((state: ITodoState) => state.items);
  // console.log("TodoItem: ", items)

  const onCompleted = (index: number) => {
    // call redux (updateCount, {count: 1, index })
  };

  return (
    <div>
      {items.map((item, index) => (
        <div>
          <span>
            <button onClick={() => onCompleted(index)}>Completed</button>
          </span>
          <span key={index}>{item.name}</span>
          <span>- ( {item.count} )</span>
        </div>
      ))}
    </div>
  );
}
