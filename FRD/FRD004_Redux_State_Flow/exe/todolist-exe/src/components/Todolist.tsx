import TodoItem from "./Todoitem";
import { Provider, useSelector } from "react-redux";
import { ITodoState } from "../redux/state";
import store from "../redux/store";
import { useState } from "react";
import "../css/style.css";
import { increment } from "../redux/actions";

export default function Todolist() {
  const [text, setText] = useState("text");
  const items = useSelector((state: ITodoState) => state.items);
  // console.log("Todolist: ", items)

  const updateText = (value: string) => {
    setText(value);
  };

  const addItem = () => {
    // call redux (increment, 'text')
    console.log("addItem");
  };

  const getDoneTaskCount = () => {
    let filtered = items.filter((item) => item.count >= 1);
    return filtered.length;
  };

  const getNewTaskCount = () => {
    let filtered = items.filter((item) => item.count === 0);
    return filtered.length;
  };

  return (
    <div className="container">
      <div>Todo List</div>
      <div>By James</div>
      <div>Count of total task: {items.length}</div>
      <div>Count of done task: {getDoneTaskCount()}</div>
      <div>Count of new task: {getNewTaskCount()} </div>
      <div>
        <input value={text} onChange={(e) => updateText(e.target.value)} />
        <button onClick={addItem}>Add</button>
      </div>

      <TodoItem />
    </div>
  );
}
