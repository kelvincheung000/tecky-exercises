import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Todolist from "./components/Todolist";
import { Provider, useSelector } from "react-redux";
import store from "./redux/store";
import Login from "./components/Login";

function App() {
  const isLoggedIn = useSelector((state: any) => state.isLoggedIn);
  console.log(isLoggedIn);

  return (
    <div className="App">
      <Login />
      {isLoggedIn && <Todolist />}
    </div>
  );
}

export default App;
