import { ITodoState } from "./state";
import { configureStore } from '@reduxjs/toolkit'
import todoItemReducer from './reducers'
const store = configureStore({ reducer: todoItemReducer })

export default store