import { createAction } from '@reduxjs/toolkit'

const increment = createAction<string>('todoItems/increment')

const login = createAction<boolean>('login/checking')

export { increment, login }


// Notes:
// The usual way to define an action in Redux is to separately 
// declare an action type constant and an action creator function 
// for constructing actions of that type.