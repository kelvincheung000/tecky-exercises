// export interface TodoItem {
//     name: string[] // => ["Buy milk", "Buy banana"]
// }

export interface ITodoState {
    items: string[]
    waterL: number
    isLoggedIn: boolean
}