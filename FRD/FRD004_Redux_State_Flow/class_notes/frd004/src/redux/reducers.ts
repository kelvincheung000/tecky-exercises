import { createReducer } from "@reduxjs/toolkit";
import { ITodoState } from "./state";
import { increment, login } from "./actions";

const initialState: ITodoState = {
    items: ["Buy milk", "Buy banana", "ABC"],
    waterL: 0,
    isLoggedIn: true
}

const todoItemReducer = createReducer(initialState, (builder) => {
    builder.addCase(login, (state, action) => {
        // action.payload => true / false
        state.isLoggedIn = action.payload
    })

    builder.addCase(increment, (state, action) => {
        // console.log("action: ", action.payload)
        state.waterL = 1000
        // [...state.items, action.payload]
    })
})

export default todoItemReducer