// import { useSelector } from "react-redux"
import { ITodoState } from "../redux/state";
import { useSelector } from "react-redux";

// interface Props {
//     items: string[]
// }

export default function TodoItem() {
  const items = useSelector((state: ITodoState) => state.items);
  // console.log("TodoItem: ", items)

  return (
    <div>
      {items.map((item, index) => (
        <div key={index}>{item}</div>
      ))}
    </div>
  );
}
