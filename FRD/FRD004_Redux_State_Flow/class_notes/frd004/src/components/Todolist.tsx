import TodoItem from "./Todoitem";
import { Provider, useSelector } from "react-redux";
import { ITodoState } from "../redux/state";
import store from "../redux/store";

export default function Todolist() {
  const items = useSelector((state: ITodoState) => state.items);
  // console.log("Todolist: ", items)
  return (
    <div>
      <TodoItem />
    </div>
  );
}
