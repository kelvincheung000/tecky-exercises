import { useState } from "react";
import TodoItem from "./TodoItem";

export default function TodoList() {
  const [text, setText] = useState<string>("input value");
  const [items, setItems] = useState<string[]>([
    "Buy milk",
    "Buy banana",
    "Buy cherry",
  ]);
  const updateText = (value: string) => {
    setText(value);
  };

  const addItem = () => {
    let newItems = items.concat(text);
    setItems(newItems);
    setText("");
    // console.log(newItems);
  };

  const onDelete = (index: number) => {
    let newItems = items.filter((item: string, i: number) => {
      return i != index;
    });
    setItems(newItems);
    //   console.log(newItems);
  };

  const [name, setName] = useState<string>("testing");
  return (
    <div>
      <h1>TodoList</h1>
      <div>
        <input value={text} onChange={(e) => updateText(e.target.value)} />
        <button onClick={() => addItem()}>Add</button>
      </div>
      <div>
        {items.map((item: string, index: number) => {
          return (
            <TodoItem
              key={index}
              name={item}
              index={index}
              onDelete={onDelete}
            />
          );
        })}
      </div>
    </div>
  );
}
