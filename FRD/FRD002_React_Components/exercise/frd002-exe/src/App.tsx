import React from "react";
import "./App.css";
import Bootstrap from "./components/Bootstrap";
import TodoList from "./components/TodoList";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <TodoList />
        <Bootstrap />
      </header>
    </div>
  );
}

export default App;
