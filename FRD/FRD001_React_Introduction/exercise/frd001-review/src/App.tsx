import AppStyle from "./App.module.css";
import icon from "./images/logo.png";
import { Helmet } from "react-helmet";

function App() {
  return (
    <div className="App">
      <Helmet>
        <meta charSet="utf-8" />
        <title>Simple Website</title>
      </Helmet>

      <header>
        <h1>Simple Website</h1>
      </header>
      <section>
        This is a simple website made without React. Try to convert this into
        React enabled.
        <ol>
          <li>
            First, you need to use
            <span className={AppStyle.command}>create-react-app</span>
          </li>
          <li>
            Second, you need to run
            <span className={AppStyle.command}>npm start</span>
          </li>
        </ol>
      </section>
      <footer>
        <img src={icon} alt="icon" />
      </footer>
    </div>
  );
}

export default App;
