import React from "react";
import logo from "./logo.svg";
import "./App.css";
import ShoppingList from "./components/ShoppingList";
import TodoList from "./components/todolist/TodoList";

// const element = <h1>Hello, world! I am Kelvin!</h1>;

// interface User {
//   firstName: string;
//   lastName: string;
//   icon: any;
// }

// const displayName = (user: User) => {
//   return `${user.firstName} - ${user.lastName}`;
// };

// const user: User = {
//   firstName: "Kelvin",
//   lastName: "Cheung",
//   icon: "",
// };

// const x = 10;
// const element = <img src="user.icon" alt="icon" />;

// function Header() {
//   return <div>I am a Header</div>;
// }

// const isShowHeader = (value: boolean): boolean => {
//   return value;
// };

// const isLoggedIn = (value: boolean): boolean => {
//   return value;
// };

const names = ["gordon", "ales", "michael"];

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <TodoList />
        {/* {<ShoppingList />} */}
        {/* The user is <b>{isLoggedIn(false) ? "currently" : "not"}</b> logged in.
        {names.map((name, index) => (
          <div>
            {name} - {index}
          </div>
        ))} */}
        {/* Hello, {displayName(user)} */}
        {/* {isShowHeader(true)} && <Header /> */}
        {/* {element} */}
        {/* <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> */}
      </header>
    </div>
  );
}

export default App;
