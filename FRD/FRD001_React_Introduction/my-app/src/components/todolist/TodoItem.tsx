import { useState } from "react";

interface Props {
  name: string;
  index: number;
  onDelete: (index: number) => void;
}

export default function TodoItem(props: Props) {
  // Declare a new state variable, which we'll call "count"
  const [count, setCount] = useState<number>(0);
  const onComplete = () => {
    setCount(count + 1);
    // console.log("onComplete");
  };

  return (
    <div>
      <button onClick={onComplete}>Complete</button>
      <span>{props.name}</span>
      <span>- ({count})</span>
      <button onClick={() => props.onDelete(props.index)}>Delete</button>
    </div>
  );
}
