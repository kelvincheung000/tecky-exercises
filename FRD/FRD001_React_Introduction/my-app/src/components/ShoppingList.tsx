import React from "react";
import ShoppingListStyle from "./ShoppingList.module.scss";

export default class ShoppingList extends React.Component {
  render() {
    return (
      <div className="shopping-list">
        <h1 className={ShoppingListStyle.text}>Shopping List</h1>
        <ul>
          <li>Instagram</li>
          <li>Whatsapp</li>
          <li>Oculus</li>
        </ul>
      </div>
    );
  }
}
