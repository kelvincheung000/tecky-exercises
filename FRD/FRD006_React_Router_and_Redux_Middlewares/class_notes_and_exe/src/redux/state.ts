import { IAuthState } from "./auth/state"
import { ITestState } from "./test/state"

export interface IRootState {
    auth: IAuthState;
    test: ITestState;
}
