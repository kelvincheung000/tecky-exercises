import { createReducer } from '@reduxjs/toolkit'
import { ITestState } from './state'

const initialState: ITestState = {
    name: "Kelvin Testing"
}

const testReducer = createReducer(initialState, (builder) => { })

export default testReducer;