import { createReducer } from '@reduxjs/toolkit'
import { loginAction } from './actions'
import { IAuthState } from './state'

const iniitalState: IAuthState = {
    isLoggedIn: false
}

const authReducer = createReducer(iniitalState, (builder) => {
    builder.addCase(loginAction, (state, action) => {
        state.isLoggedIn = action.payload
    })
})

export default authReducer;