import { createAction } from '@reduxjs/toolkit'

// createAction is a helper function for defining a Redux action type and creator.
const loginAction = createAction<boolean>('auth/checking')

export { loginAction }