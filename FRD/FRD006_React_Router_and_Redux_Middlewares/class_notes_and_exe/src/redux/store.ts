import { configureStore } from '@reduxjs/toolkit'
import rootReducer from './test/reducers'
import authReducer from './auth/reducers'
import logger from "redux-logger"

// The store now has redux-thunk added and the Redux DevTools Extension is turned on
const store = configureStore({
    reducer: {
        test: rootReducer,
        auth: authReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
    // Store has all of the default middleware added, _plus_ the logger middleware
})

export default store;