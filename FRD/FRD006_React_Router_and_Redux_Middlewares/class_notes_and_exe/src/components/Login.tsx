import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Routes, Route, useParams } from "react-router-dom";
import { loginAction } from "../redux/auth/actions";

function Login() {
  let { id } = useParams();
  // The useParams hook returns an object of key/value pairs of the dynamic params from
  // the current URL that were matched by the <Route path>
  // Child routes inherit all params from their parent routes.

  const dispatch = useDispatch();
  // This hook returns a reference to the dispatch function from the Redux store.
  // You may use it to dispatch actions as needed.

  const [username, setUsername] = useState<string>("kelvin");
  const [password, setPassword] = useState<string>("1234");
  // Call the useState Hook directly inside our component
  // Declare a new state variable, i.e. username and password
  // useState is a new way to use the exact same capabilities that this.state provides in a class
  // It returns a pair of values: the current state and a function that updates it.

  const onSubmit = (e: any) => {
    e.preventDefault();
    if (username === "kelvin" && password === "1234") {
      dispatch(loginAction(true));
      // Dispatches an action and this is the only way to trigger a state change.
    } else {
      dispatch(loginAction(false));
    }
  };

  return (
    <div>
      <h1>Login Page - {id}</h1>
      <form onSubmit={onSubmit}>
        <div>
          <label>
            Username:
            <input
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            ></input>
          </label>
        </div>

        <div>
          <label>
            Password:
            <input
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></input>
          </label>
        </div>
      </form>
    </div>
  );
}

export default Login;
