import React from "react";
import { Outlet } from "react-router-dom";
import { Routes, Route, useParams, useNavigate } from "react-router-dom";

function Dashboard() {
  let navigate = useNavigate();
  let { id } = useParams();

  return (
    <div>
      <h1>Dashboard - {id}</h1>
      <nav>
        <button type="button" onClick={() => navigate("/")}>
          Dashboard
        </button>
        <button type="button" onClick={() => navigate("/messages")}>
          Messages
        </button>
        <button type="button" onClick={() => navigate("/tasks")}>
          Tasks
        </button>
        <button type="button" onClick={() => navigate("/about")}>
          About
        </button>
      </nav>
      <Outlet />
    </div>
  );
}

export default Dashboard;
