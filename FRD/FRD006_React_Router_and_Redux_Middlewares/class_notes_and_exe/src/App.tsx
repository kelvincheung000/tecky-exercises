import React from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Route, Routes, Link, Outlet } from "react-router-dom";
import "./App.css";
import { IRootState } from "./redux/state";
import Login from "./components/Login";
import MainPage from "./components/MainPage";
import Dashboard from "./components/Dashboard";
import Error404 from "./components/Error404";

function App() {
  const isLoggedIn = useSelector((state: IRootState) => state.auth.isLoggedIn);
  const name = useSelector((state: IRootState) => state.test.name);

  return (
    <div className="App">
      <BrowserRouter>
        <nav>
          <Link to="abc">Dashboard</Link>
          <br />
          <Link to="abc/login">Login Page</Link>
          <br />
          <Link to="main">Main Page</Link>
        </nav>

        <Routes>
          <Route path="abc/:id" element={<Dashboard />}>
            <Route path="login" element={<Login />}></Route>
          </Route>
          <Route path="main" element={<MainPage />}></Route>
          <Route path="*" element={<Error404 />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
