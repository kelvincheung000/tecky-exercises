import { useState } from "react";
import "./App.css";
// import FunctionLifecycle from './components/FunctionLifecycle';
// import GenForm from './components/GenForm';
// import HookForm from './components/HookForm';
import Lifecycle from "./Components/Lifecycle";

function App() {
  const [isShowLifecycle, setIsShowLifecycle] = useState<boolean>(false);
  const [text, setText] = useState<string>("Show");

  return (
    <div className="App">
      {/* <button onClick={() => setIsShowLifecycle(!isShowLifecycle)}>{text}</button> */}
      {/* {isShowLifecycle && <Lifecycle/> } */}
      {/* {isShowLifecycle && <FunctionLifecycle/>} */}
      {/* <GenForm/> */}
    </div>
  );
}

export default App;
