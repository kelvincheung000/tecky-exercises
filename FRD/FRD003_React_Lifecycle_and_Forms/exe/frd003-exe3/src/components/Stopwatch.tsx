import { useState } from "react";
import "../css/style.css";
export default function Stopwatch() {
  enum Mode {
    Start = "Start",
    Stop = "Stop",
  }

  const INVAL = 80;
  const [milliseconds, setMilliseconds] = useState<number>(1001);
  const [intervalId, setIntervalId] = useState<NodeJS.Timer>();
  const [watchStatus, setWatchStatus] = useState<Mode>(Mode.Stop);
  const [timeArray, setTimeArray] = useState<string[]>([]);

  const [millisecondsForLap, setMillisecondsForLap] = useState<number>(1001);

  const getMilliseconds = (milliseconds: number) => {
    return (milliseconds % 1000).toString().padStart(3, "0");
  };
  const getSeconds = (milliseconds: number) => {
    return (Math.floor(milliseconds / 1000) % 60).toString().padStart(2, "0");
  };

  const getMins = (milliseconds: number) => {
    return Math.floor(milliseconds / 1000 / 60)
      .toString()
      .padStart(2, "0");
  };

  const displayTimerForlap = () => {
    return `${getMins(millisecondsForLap)}, ${getSeconds(
      millisecondsForLap
    )}, ${getMilliseconds(millisecondsForLap)}`;
  };

  const displayTimer = () => {
    return `${getMins(milliseconds)}, ${getSeconds(
      milliseconds
    )}, ${getMilliseconds(milliseconds)}`;
  };

  const start = () => {
    let inId = setInterval(() => {
      setMilliseconds((value) => value + INVAL);
      setMillisecondsForLap((value) => value + INVAL);
    }, INVAL);

    setIntervalId(inId);
    setWatchStatus(Mode.Start);
  };

  const pause = () => {
    clearInterval(intervalId);
    setWatchStatus(Mode.Stop);
  };

  const lap = () => {
    let newTimeArray = [...timeArray, displayTimerForlap()];
    setTimeArray(newTimeArray);
    setMillisecondsForLap(0);
  };

  const reset = () => {
    setMilliseconds(0);
    clearInterval(intervalId);
  };

  const handleLeftBtn = () => {
    if (watchStatus == Mode.Start) {
      pause();
    } else {
      start();
    }
  };

  const handleRightBtn = () => {
    if (watchStatus == Mode.Start) {
      lap();
    } else {
      reset();
    }
  };

  return (
    <div className="stopwatch">
      <h3>Stop Watch</h3>
      <hr />
      <h1>{displayTimer()}</h1>
      <h2>{displayTimerForlap()}</h2>
      <hr></hr>
      <div className="buttonGroup">
        <button onClick={handleLeftBtn}>
          {/* Start */}
          {/* Pause */}
          {watchStatus === Mode.Stop ? "Start" : "Pause"}
        </button>
        <button onClick={handleRightBtn}>
          {/* Reset */}
          {/* Lap */}
          {watchStatus === Mode.Stop ? "Reset" : "Lap"}
        </button>
      </div>
      <div>
        {timeArray.length > 0 && (
          <div>
            {timeArray.map((time, index) => {
              return <div key={index}>{time}</div>;
            })}
          </div>
        )}
      </div>
    </div>
  );
}
