import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "../css/style.css";
interface FormState {
  seconds: number;
}

export default function Countdown() {
  const { register, watch, getValues } = useForm<FormState>({
    defaultValues: {
      seconds: 1,
    },
  });
  enum Mode {
    Start = "Start",
    Stop = "Stop",
  }

  const INVAL = 80;
  const [milliseconds, setMilliseconds] = useState<number>(9999);
  const [intervalId, setIntervalId] = useState<NodeJS.Timer>();
  const [watchStatus, setWatchStatus] = useState<Mode>(Mode.Stop);
  const [timeArray, setTimeArray] = useState<string[]>([]);

  //   const [millisecondsForLap, setMillisecondsForLap] = useState<number>(1001);

  //   useEffect(() => {
  //     let seconds = getValues("seconds");
  //     if (seconds > 0) {
  //       setMilliseconds(seconds * 1000);
  //     }
  //   }, []);

  useEffect(() => {
    resume();
    watch((data) => {
      let seconds = data.seconds;
      if (seconds) {
        setMilliseconds(seconds * 1000);
      }
    });
  }, []);
  const getMilliseconds = (milliseconds: number) => {
    return (milliseconds % 1000).toString().padStart(3, "0");
  };
  const getSeconds = (milliseconds: number) => {
    return (Math.floor(milliseconds / 1000) % 60).toString().padStart(2, "0");
  };

  const getMins = (milliseconds: number) => {
    return Math.floor(milliseconds / 1000 / 60)
      .toString()
      .padStart(2, "0");
  };

  //   const displayTimerForlap = () => {
  //     return `${getMins(millisecondsForLap)}, ${getSeconds(
  //       millisecondsForLap
  //     )}, ${getMilliseconds(millisecondsForLap)}`;
  //   };

  const displayTimer = () => {
    return `${getMins(milliseconds)}, ${getSeconds(
      milliseconds
    )}, ${getMilliseconds(milliseconds)}`;
  };

  const start = () => {
    if (milliseconds == 0) {
      resume();
    }
    let inId = setInterval(() => {
      setMilliseconds((value) => {
        let newValue = value - INVAL;
        return newValue < 0 ? 0 : newValue;
      });
    }, INVAL);

    setIntervalId(inId);
    setWatchStatus(Mode.Start);
  };

  const pause = () => {
    clearInterval(intervalId);
    setWatchStatus(Mode.Stop);
  };

  //   const lap = () => {
  //     let newTimeArray = [...timeArray, displayTimerForlap()];
  //     setTimeArray(newTimeArray);
  //     // setMillisecondsForLap(0);
  //   };

  const reset = () => {
    setMilliseconds(0);
    clearInterval(intervalId);
  };

  const resume = () => {
    let seconds = getValues("seconds");
    if (seconds > 0) {
      setMilliseconds(seconds * 1000);
    }
    clearInterval(intervalId);
    setWatchStatus(Mode.Stop);
  };

  const handleLeftBtn = () => {
    if (watchStatus == Mode.Start) {
      pause();
    } else {
      start();
    }
  };

  const handleRightBtn = () => {
    resume();
  };

  return (
    <div className="stopwatch">
      <h3>Count Down</h3>
      <hr />
      <h1>{displayTimer()}</h1>
      <input type="number" {...register("seconds")}></input>秒<hr></hr>
      <div className="buttonGroup">
        <button onClick={handleLeftBtn}>
          {/* Start */}
          {/* Pause */}
          {watchStatus === Mode.Stop ? "Start" : "Pause"}
        </button>
        <button onClick={handleRightBtn}>
          {/* Reset */}
          {/* Lap */}
          {watchStatus === Mode.Stop ? "Reset" : "Resume"}
        </button>
      </div>
      <div>
        {timeArray.length > 0 && (
          <div>
            {timeArray.map((time, index) => {
              return <div key={index}>{time}</div>;
            })}
          </div>
        )}
      </div>
    </div>
  );
}
