import React from "react";
import "./App.css";
import Countdown from "./components/Countdown";
import Stopwatch from "./components/Stopwatch";

function App() {
  return (
    <div className="App">
      {/* <Stopwatch /> */}
      <Countdown />
    </div>
  );
}

export default App;
